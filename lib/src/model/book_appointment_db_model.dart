import 'book_app_attendee_db_model.dart';

final String tblBookAppointment = 'tbl_bookAppointment';

class BookAppointmentFields {
  static final List<String> values = [
    /// Add all fields
    id, docId, user, syncStatus, city, region, appDate, slotId, contactPerson,
    landmark, paidFlag, token, appType, password, address, cancellationReason, requestNo, env,
    appointmentId, appStatus, status, amount, startTime,userName,userContact,userEmail, payStatus, amountToPay, addAttendeeList
  ];

  static final String id = 'id';
  static final String docId = 'docId';
  static final String user = 'user';
  static final String syncStatus = 'syncStatus';
  static final String city = 'city';//
  static final String region = 'region';//
  static final String appDate = 'appDate';//
  static final String slotId = 'slotId';//
  static final String contactPerson = 'contactPerson';//
  static final String landmark = 'landmark';//
  static final String paidFlag = 'paidFlag';
  static final String token = 'token';
  static final String appType = 'appType';
  static final String password = 'password';
  static final String address = 'address';//
  static final String cancellationReason = 'cancellationReason';//
  static final String requestNo = 'request_no';
  static final String env = 'env';
  static final String appointmentId = 'appointment_id';
  static final String appStatus = 'app_status';
  static final String status = 'status';
  static final String amount = 'amount';
  static final String startTime = 'startTime';
  static final String userName = 'userName';
  static final String userContact = 'userContact';
  static final String userEmail = 'userEmail';
  static final String payStatus = 'payStatus';
  static final String amountToPay = 'amountToPay';
  static final String addAttendeeList = 'addAttendeeList';
}

class BookAppointmentDBModel{
  final int? id;
  final String? docId;
  final String? user;
  final int? syncStatus;
  final String? city;
  final String? region;
  String? appDate;
  final String? slotId;
  final String? contactPerson;
  final String? landmark;
  final int? paidFlag;
  final int? token;
  final int? appType;
  final String? password;
  final String? address;
  final String? cancellationReason;
  final int? requestNo;
  final String? env;
  final int? appointmentId;
  final int? appStatus;
  final int? status;
  final int? amount;
  final String? startTime;
  final String? userName;
  final String? userContact;
  final String? userEmail;
  final int? payStatus;
  final int? amountToPay;
  List<BookAppAttendeeDBModel>? addAttendeeList;

  BookAppointmentDBModel({
    this.id,
    required this.docId,
    required this.user,
    required this.syncStatus,
    required this.city,
    required this.region,
    required this.appDate,
    this.slotId,
    required this.contactPerson,
    required this.landmark,
    this.paidFlag,
    this.token,
    this.appType,
    required this.password,
    required this.address,
    this.cancellationReason,
    this.requestNo,
    this.env,
    this.appointmentId,
    required this.appStatus,
    required this.status,
    this.amount,
    required this.startTime,
    required this.userName,
    required this.userContact,
    required this.userEmail,
    required this.payStatus,
    this.amountToPay,
    this.addAttendeeList,
  });

  static BookAppointmentDBModel fromJson(Map<String, Object?> json) => BookAppointmentDBModel(
    id: json[BookAppointmentFields.id] as int,
    docId: json[BookAppointmentFields.docId] as String?,
    user: json[BookAppointmentFields.user] as String?,
    syncStatus: json[BookAppointmentFields.syncStatus] as int,
    city: json[BookAppointmentFields.city] as String,
    region: json[BookAppointmentFields.region] as String,
    appDate: json[BookAppointmentFields.appDate] as String,
    slotId: json[BookAppointmentFields.slotId] as String?,
    contactPerson: json[BookAppointmentFields.contactPerson] as String,
    landmark: json[BookAppointmentFields.landmark] as String,
    paidFlag: json[BookAppointmentFields.paidFlag] as int?,
    token: json[BookAppointmentFields.token] as int?,
    appType:  json[BookAppointmentFields.appType] as int?,
    password:  json[BookAppointmentFields.password] as String,
    address:  json[BookAppointmentFields.address] as String,
    cancellationReason:  json[BookAppointmentFields.cancellationReason] as String?,
    requestNo: json[BookAppointmentFields.requestNo] as int?,
    env: json[BookAppointmentFields.env] as String?,
    appointmentId:  json[BookAppointmentFields.appointmentId] as int?,
    appStatus: json[BookAppointmentFields.appStatus] as int,
    status: json[BookAppointmentFields.status] as int,
    amount: json[BookAppointmentFields.amount] as int,
    startTime: json[BookAppointmentFields.startTime] as String?,
    userName: json[BookAppointmentFields.userName] as String?,
    userContact: json[BookAppointmentFields.userContact] as String?,
    userEmail: json[BookAppointmentFields.userEmail] as String?,
    payStatus: json[BookAppointmentFields.payStatus] as int?,
    amountToPay: json[BookAppointmentFields.amountToPay] as int?,
    addAttendeeList: json[BookAppointmentFields.addAttendeeList] as List<BookAppAttendeeDBModel>?,
  );

  Map<String, Object?> toJson() => {
    BookAppointmentFields.id: id,
    BookAppointmentFields.docId: docId,
    BookAppointmentFields.user: user,
    BookAppointmentFields.syncStatus: syncStatus,
    BookAppointmentFields.city: city,
    BookAppointmentFields.region: region,
    BookAppointmentFields.appDate: appDate,
    BookAppointmentFields.slotId: slotId,
    BookAppointmentFields.contactPerson: contactPerson,
    BookAppointmentFields.landmark: landmark,
    BookAppointmentFields.paidFlag: paidFlag,
    BookAppointmentFields.token: token,
    BookAppointmentFields.appType: appType,
    BookAppointmentFields.password: password,
    BookAppointmentFields.address: address,
    BookAppointmentFields.cancellationReason: cancellationReason,
    BookAppointmentFields.requestNo: requestNo,
    BookAppointmentFields.env: env,
    BookAppointmentFields.appointmentId: appointmentId,
    BookAppointmentFields.appStatus: appStatus,
    BookAppointmentFields.status:status,
    BookAppointmentFields.amount:amount,
    BookAppointmentFields.startTime:startTime,
    BookAppointmentFields.userName:userName,
    BookAppointmentFields.userContact:userContact,
    BookAppointmentFields.userEmail:userEmail,
    BookAppointmentFields.payStatus: payStatus,
  };

}