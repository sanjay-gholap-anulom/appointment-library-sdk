
class MsgConstants {
  static const String logOut = 'Are you sure to logout?';
  static const String checkUserLogin = 'Are you an existing user?';
  static const String cancelSuccess = 'Appointment Cancelled successfully';
  static const String rescheduleSuccess = 'Appointment Rescheduled successfully';

}