final String tblBookAppAttendee = 'tbl_BookAppAttendee';

class BookAppAttendeeFields {
  static final List<String> values = [
    /// Add all fields
    id, partyType, env, name, email, contact, appointmentId, documentId
  ];

  static final String id = 'id';
  static final String partyType = 'party_type';
  static final String env = 'env';
  static final String name = 'name';
  static final String email = 'email';
  static final String contact = 'contact';
  static final String address = 'address';
  static final String appointmentId = 'appointment_id';
  static final String documentId = 'document_id';
}

class BookAppAttendeeDBModel {
  final int? id;
  final int? partyType;
  late String? env;
  final String name;
  final String email;
  final String contact;
  final String? address;
  final int? appointmentId;
  final int? documentId;

   BookAppAttendeeDBModel({
    this.id,
     this.partyType,
     this.env,
    required this.name,
    required this.email,
    required this.contact,
     this.address,
    this.appointmentId,
    this.documentId,
  });


  static BookAppAttendeeDBModel fromJson(Map<String, Object?> json) => BookAppAttendeeDBModel(
    id: json[BookAppAttendeeFields.id] as int,
    partyType: json[BookAppAttendeeFields.partyType] as int?,
    env: json[BookAppAttendeeFields.env] as String?,
    name: json[BookAppAttendeeFields.name] as String,
    email: json[BookAppAttendeeFields.email] as String,
    contact: json[BookAppAttendeeFields.contact] as String,
    address: '',//json[BookAppAttendeeFields.address] as String,
    appointmentId: json[BookAppAttendeeFields.appointmentId] as int?,
    documentId: json[BookAppAttendeeFields.documentId] as int?,
  );

  Map<String, Object?> toJson() => {
    BookAppAttendeeFields.id: id,
    BookAppAttendeeFields.partyType: partyType,
    BookAppAttendeeFields.env: env,
    BookAppAttendeeFields.name: name,
    BookAppAttendeeFields.email: email,
    BookAppAttendeeFields.contact: contact,
    BookAppAttendeeFields.address: address,
    BookAppAttendeeFields.appointmentId: appointmentId,
    BookAppAttendeeFields.documentId: documentId,
  };
}
