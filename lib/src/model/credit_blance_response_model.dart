class ChargesDetailsResponseModel {
  int? status;
  double? creditBalance;
  int? appointmentCharges;

  ChargesDetailsResponseModel(
      {this.status, this.creditBalance, this.appointmentCharges});

  ChargesDetailsResponseModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    creditBalance = json['credit_balance'];
    appointmentCharges = json['appointment_charges'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['credit_balance'] = this.creditBalance;
    data['appointment_charges'] = this.appointmentCharges;
    return data;
  }
}

