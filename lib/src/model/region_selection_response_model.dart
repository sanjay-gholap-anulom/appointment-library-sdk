class RegionResponseModel {
  int? status;
  List<Regions>? regions;

  RegionResponseModel({this.status, this.regions});

  RegionResponseModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    if (json['regions'] != null) {
      regions = <Regions>[];
      json['regions'].forEach((v) {
        regions!.add(new Regions.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.regions != null) {
      data['regions'] = this.regions!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Regions {
  String? regionName;
  int? divisionId;
  int? regionId;

  Regions({this.regionName, this.divisionId, this.regionId});

  Regions.fromJson(Map<String, dynamic> json) {
    regionName = json['region_name'];
    divisionId = json['division_id'];
    regionId = json['region_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['region_name'] = this.regionName;
    data['division_id'] = this.divisionId;
    data['region_id'] = this.regionId;
    return data;
  }
}