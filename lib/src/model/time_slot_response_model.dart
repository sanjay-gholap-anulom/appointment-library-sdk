class TimeSlotResponseModel {
  String? status;
  List<TimeSlot>? timeSlot;

  TimeSlotResponseModel({this.status, this.timeSlot});

  TimeSlotResponseModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    if (json['time_slot'] != null) {
      timeSlot = <TimeSlot>[];
      json['time_slot'].forEach((v) {
        timeSlot!.add(new TimeSlot.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.timeSlot != null) {
      data['time_slot'] = this.timeSlot!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class TimeSlot {
  bool? available;
  bool? discount;
  String? cStartTime;
  String? cEndTime;
  int? slotId;
  int? block;

  TimeSlot(
      {this.available,
        this.discount,
        this.cStartTime,
        this.cEndTime,
        this.slotId,
        this.block});

  TimeSlot.fromJson(Map<String, dynamic> json) {
    available = json['available'];
    discount = json['discount'];
    cStartTime = json['c_start_time'];
    cEndTime = json['c_end_time'];
    slotId = json['slot_id'];
    block = json['block'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['available'] = this.available;
    data['discount'] = this.discount;
    data['c_start_time'] = this.cStartTime;
    data['c_end_time'] = this.cEndTime;
    data['slot_id'] = this.slotId;
    data['block'] = this.block;
    return data;
  }
}