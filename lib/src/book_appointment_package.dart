
import 'dart:core';
import 'dart:io';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:book_appointment_package/src/book_new_appoint_provider.dart';
import 'package:book_appointment_package/src/utils/app_constants.dart';
import 'package:book_appointment_package/src/utils/color_constants.dart';
import 'package:book_appointment_package/src/utils/custom_toast.dart';
import 'package:book_appointment_package/src/utils/image_path_constants.dart';
import 'package:book_appointment_package/src/utils/sharedpreference_utils.dart';
import 'package:book_appointment_package/src/utils/text_styles.dart';
import 'package:book_appointment_package/src/widgets/custom_button.dart';
import 'package:book_appointment_package/src/widgets/custom_radio_button_list.dart';
import 'package:connectivity/connectivity.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';
import 'custom_dialog_box.dart';
import 'custom_dialog_box_provider.dart';
import 'model/book_app_attendee_db_model.dart';
import 'model/book_appointment_db_model.dart';
import 'model/credit_blance_response_model.dart';
import 'model/message_constants.dart';
import 'package:modal_progress_hud_nsn/modal_progress_hud_nsn.dart';

class BookNewAppPackageView extends StatefulWidget{
  /*final citySelection;
  final regionSelection;
  final selectedDate;
  final timeSlotValue;
  final selectedTimeSlot;
  final selectedDateInDateFormat;
  final divisionId;
  final  List<CityAreaSelection> regionList;
  final List<TimeslotSelection> timeSlotList;
  final slotId;

  final token;
  final password;
  final List<BookAppAttendeeDBModel> attendeeList;
  final List<BookAppAttendeeDBModel> contactPersonList;
  final selectedCP;
  final buildingType;
  final addressType;
  final buildingName;
  final cityState;
  final landmark;
  final navtigationPage;*/

  const BookNewAppPackageView({Key? key, /*required this.citySelection, required this.regionSelection, required this.selectedDate, required this.timeSlotValue,  required this.selectedTimeSlot, required this.selectedDateInDateFormat,
    required this.divisionId, required this.regionList, required this.timeSlotList, required this.slotId, required this.token, required this.password,
    required this.attendeeList, required this.contactPersonList, required this.selectedCP, required this.buildingType, required this.addressType, required this.buildingName,
    required this.cityState, required this.landmark,required this.navtigationPage*/
  });
  _BookNewAppViewState createState() => _BookNewAppViewState();
}

class _BookNewAppViewState extends State<BookNewAppPackageView>{
  BookNewAppProvider _provider = BookNewAppProvider();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  final GlobalKey<FormState> _formDialogKey = new GlobalKey<FormState>();

  final _dateController = TextEditingController();
  final _tokenNoController = TextEditingController();
  final _passwordController = TextEditingController();
  final _cityController = TextEditingController();
  final _regionController = TextEditingController();
  final _attendeeController = TextEditingController();
  final _buildingNameController = TextEditingController();
  final _landmarkController = TextEditingController();
  final _cityStateNameController = TextEditingController();
  final _alertNameController = TextEditingController();
  final _alertMailController = TextEditingController();
  final _alertAttContactController = TextEditingController();
  final _alertAddressController = TextEditingController();
  String selectedRegion = '' ;
  String selectedCity = '';
  String documentId = '';
  DateTime? currentBackPressTime;
  // final focus = FocusNode();
  RegExp regex = RegExp(r"([.]*0)(?!.*\d)");


  @override
  void initState() {
  /*  if(widget.regionSelection != '') {
      selectedRegion = widget.regionSelection;
      selectedCity = widget.citySelection;
      setState(() {
        _dateController.text = widget.selectedDate+ ' '+widget.timeSlotValue;
        _cityController.text = selectedCity;
        _regionController.text = selectedRegion;
        _tokenNoController.text= widget.token;
        _passwordController.text = widget.password;
        _provider.addAttendeeList.addAll(widget.attendeeList);
        _provider.contactPersonList.addAll(widget.contactPersonList);
        _provider.selectedCPName = widget.selectedCP;
        _provider.groupBuildingType = widget.buildingType;
        _provider.groupAddressType = widget.addressType;
        _buildingNameController.text = widget.buildingName;
        _cityStateNameController.text = widget.cityState;
        _landmarkController.text = widget.landmark;

      });

    }*/
    getChargesDetails();
    super.initState();

  }

  @override
  Widget build(BuildContext context) {
    final bottom = MediaQuery.of(context).viewInsets.bottom;
    return ChangeNotifierProvider<BookNewAppProvider>(
      create: (context) => _provider,
      child: Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          leading:Padding(
            padding: const EdgeInsets.only(left: 8.0),
            child: IconButton(
              icon:/*widget.navtigationPage == 'drawer'? Image.asset(ImagePath.homePageMenu) : */Image.asset(ImagePath.leftArrowIcon)  ,
              onPressed: () async {
                /*   String? email = await SharedPreferenceUtils.getStringPreference(
                          SharedPreferenceUtils.EMAIL_ID);*/
                if(_provider.email == '' ){
                  CustomToast().customToast('Please login first');
                }
                /*else if(widget.navtigationPage == 'drawer'){
                  _scaffoldKey.currentState!.openDrawer(); // do something

                }*/
                else {
                  Navigator.of(context).pop();
                }
              },
            ),
          ),

          title: Text(
            AppConstants.bookNewAppText, style: titleHeadline,),

          backgroundColor: Colors.transparent,
          elevation: 0.0,
        ),
        //drawer: const DrawerPage(),
        resizeToAvoidBottomInset: false,
        body:  Consumer<BookNewAppProvider>(builder: (context, model, child) {
          return  ModalProgressHUD(
            inAsyncCall: model.isLoading,
            child:
            SafeArea(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                      child: Form(
                          key: _formKey,
                          child:
                          SingleChildScrollView(
                              scrollDirection: Axis.vertical,
                              //  reverse: true,
                              child: Padding(
                                  padding: EdgeInsets.only(bottom: bottom),
                                  child: Column(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        _titleText(AppConstants.selectDateTimeText),

                                        Padding(
                                          padding: const EdgeInsets.only(left: 20.0,right: 20),
                                          child: GestureDetector(
                                            onTap: () =>{
                                              showCustomDialog(),
                                              /*  showDialog(context: context,
                                        barrierColor: Colors.white.withOpacity(0),
                                        builder: (BuildContext context){
                                          return
                                            Container(
                                              color: AppColors.colorGrey,
                                              height: double.infinity,
                                              width: double.infinity,
                                              child: CustomDialogBox(

                                                citySelection: widget.citySelection ,
                                                regionSelection: widget.regionSelection ,
                                                selectedDate: widget.selectedDate,
                                                timeSlotValue: widget.timeSlotValue,
                                                selectedTimeSlot: widget.selectedTimeSlot,
                                                selectedDateInDateFormat: widget.selectedDateInDateFormat,
                                                divisionId: widget.divisionId,
                                                regionList : widget.regionList,
                                                timeSlotList: widget.timeSlotList,
                                                slotId: widget.slotId,
                                              ),);
                                        }),*/
                                            } ,
                                            child: AbsorbPointer(
                                              child: TextFormField(
                                                // textInputAction: TextInputAction.next,
                                                autovalidateMode: AutovalidateMode.onUserInteraction,
                                                controller: _dateController,
                                                decoration: InputDecoration(
                                                  suffixIcon: Container(
                                                    padding: EdgeInsets.all(8),
                                                    height: 5,
                                                    width: 5,
                                                    child: Image(
                                                        image: AssetImage(
                                                            ImagePath.bookAppointmentCalender)),),
                                                  hintText: 'DD/MM/YYYY',
                                                ),
                                                keyboardType: TextInputType.datetime,
                                                validator: (value) {
                                                  if (value!.isEmpty || value == null) {
                                                    return AppConstants.dateErrorText;
                                                  }
                                                },
                                              ),
                                            ),
                                          ),
                                        ),

                                        _titleText(AppConstants.selectCityText),
                                        Padding(
                                          padding: const EdgeInsets.only(left: 20.0,right: 20),
                                          child: GestureDetector(
                                            onTap: () =>{
                                              showCustomDialog(),
                                            } ,
                                            child: AbsorbPointer(
                                              child: TextFormField(
                                                // textInputAction: TextInputAction.next,
                                                autovalidateMode: AutovalidateMode.onUserInteraction,
                                                controller: _cityController,
                                                decoration: InputDecoration(
                                                  hintText: AppConstants.selectCityText,
                                                ),
                                                validator: (value) {
                                                  if (value!.isEmpty || value == null) {
                                                    return AppConstants.cityErrorText;
                                                  }
                                                },
                                              ),
                                            ),
                                          ),
                                        ),
                                        _titleText(AppConstants.selectRegionText),
                                        Padding(
                                          padding: const EdgeInsets.only(left: 20.0,right: 20),
                                          child: GestureDetector(
                                            onTap: () =>{
                                              showCustomDialog(),
                                            } ,
                                            child: AbsorbPointer(
                                              child: TextFormField(
                                                //    textInputAction: TextInputAction.next,
                                                controller: _regionController,
                                                autovalidateMode: AutovalidateMode.onUserInteraction,
                                                decoration: InputDecoration(
                                                  hintText: AppConstants.selectRegionText,
                                                ),
                                                validator: (value) {
                                                  if (value!.isEmpty || value == null) {
                                                    return AppConstants.regionErrorText;
                                                  }
                                                },
                                              ),
                                            ),
                                          ),
                                        ),

                                        _titleText(AppConstants.tokenNumberText),
                                        Visibility(
                                          visible: true,
                                          child: Padding(
                                            padding: const EdgeInsets.only(left: 20.0,right: 20),
                                            child: TextFormField(
                                              textInputAction: TextInputAction.next,

                                              //autovalidateMode: AutovalidateMode.onUserInteraction,
                                              controller: _tokenNoController,
                                              keyboardType: TextInputType.number,
                                              inputFormatters: [FilteringTextInputFormatter.digitsOnly],//[WhitelistingTextInputFormatter.digitsOnly],
                                              maxLength: 14,
                                              decoration: InputDecoration(
                                                hintText: AppConstants.tokenNumberText,
                                              ),
                                              validator: (value) {
                                                if (value!.isEmpty || value == null || value.length!=14) {
                                                  return AppConstants.tokenNoErrorText;
                                                }
                                              },
                                            ),
                                          ),
                                        ),
                                        _titleTextPassword(AppConstants.passwordText),
                                        Padding(
                                          padding: const EdgeInsets.only(left: 20.0,right: 20),
                                          child: TextFormField(
                                            textInputAction: TextInputAction.done,
                                            controller: _passwordController,
                                            obscureText: model.obscureText,
                                            enableSuggestions: false,
                                            autocorrect: false,
                                            decoration: InputDecoration(
                                              hintText: AppConstants.passwordText,

                                              suffixIcon: GestureDetector(
                                                onTap: () {
                                                  setState(() {
                                                    model.obscureText = !model.obscureText;
                                                  });
                                                },
                                                child: Container(
                                                    child: model.obscureText
                                                        ? IconButton(icon: Image.asset(ImagePath.passwordHideIcon,width: 24.0,height: 24.0,), onPressed: null,)
                                                        :IconButton(icon: Image.asset(ImagePath.passwordShowIcon,width: 24.0,height: 24.0,), onPressed: null,)
                                                ),
                                              ),
                                            ),
                                            validator: (value) {
                                              if (value!.isEmpty || value == null) {
                                                return AppConstants.passwordErrorText;
                                              }
                                            },
                                          ),
                                        ),
                                        SizedBox(height:10,),
                                        //_titleText(AppConstants.attendeeNameText),
                                        _otherAttendeesTitle(),
                                        _addOtherAttendees(),
                                        _titleText(AppConstants.selectContactPersonText),
                                        _selectContactPerson(model),
                                        _addressDetails(model),
                                        if(model.email!= '')...[
                                          _billingInformationDetails(model),
                                        ],]))))),


                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 20),
                    child: CustomButton(
                      height: 58,
                      minWidth: MediaQuery.of(context).size.width*0.9,
                      buttonName:
                      (_provider.email == '' || model.amountToPay == 0) ?AppConstants.submitText
                          :
                      AppConstants.payText +' ${model.amountToPay} '+AppConstants.inrText,
                        onPressed: () async {
                        model.isLoading = true;
                        if (_formKey.currentState!.validate()) {
                         if(model.addAttendeeList.length == 0){
                            CustomToast().customToast(AppConstants.pleaseAddAttendeeError);
                          }
                          else if(model.selectedCPName == ''){
                            CustomToast().customToast(AppConstants.selectContactPerError);
                          }
                          else if(model.groupBuildingType == 0){
                            CustomToast().customToast(AppConstants.aptmSelectBuildingError);
                          }
                          else if(model.groupAddressType == 0){
                            CustomToast().customToast(AppConstants.aptmAddressTypeError);
                          }
                          else if(model.groupAddressType == 2 && _buildingNameController.text.isEmpty){
                            CustomToast().customToast(AppConstants.aptmBuildingNameError);
                          }
                          else if(model.groupAddressType == 2 && _cityStateNameController.text.isEmpty){
                            CustomToast().customToast(AppConstants.aptmCityStateError);
                          }
                          else{
                            _provider.isLoading = true;
                            String contactPerson =  _provider.selectedContactPerson!.name +','+_provider.selectedContactPerson!.email+','+_provider.selectedContactPerson!.contact;
                            String address = "";
                            if(_provider.groupAddressType == 1){
                              address = _provider.selectedContactPerson!.address!;
                            }
                            else if(_provider.groupAddressType == 2){
                              address = _buildingNameController.text.toString() + ' '+ _cityStateNameController.text.toString();
                            }
                            var endDate = _provider.navigationModel.selectedDateInDateFormat;
                            print("end date is $endDate");
                            var bookAppointmentDBModel = BookAppointmentDBModel(
                                user: _provider.email,// == null? 'nishigandha.joshi@anulom.com' : _email,
                                docId: documentId,
                                syncStatus: 0,
                                city: selectedCity,
                                region: selectedRegion,
                                appDate: _provider.appDate,//widget.selectedDate+widget.timeSlotValue,
                                slotId: _provider.navigationModel.slotId.toString(),
                                contactPerson: contactPerson,
                                landmark: _landmarkController.text,
                                paidFlag: 0,
                                token: int.parse(_tokenNoController.text),
                                appType: 0,
                                password: _passwordController.text,
                                address: address,
                                cancellationReason: 'State some valid reason here',
                                requestNo: 0,
                                env: '',
                                appointmentId: 0,
                                appStatus: 0,
                                status: 0,
                                amount: _provider.email == ''? 0:_provider.appointmentCharges,
                                startTime:_provider.navigationModel.selectedDateInDateFormat.toString(),
                                userName:'', userContact: '', userEmail: '',payStatus : 0,
                                amountToPay: _provider.amountToPay,
                                addAttendeeList: _provider.addAttendeeList,
                            );
                            //Navigator.of(context).pop();
                        //Navigator.of(context)
                      //  pushNamedAndRemoveUntil(context,  "/home", ModalRoute.withName('/'), arguments: bookAppointmentDBModel);
                     print('book appmodel ${bookAppointmentDBModel.token}');
                        Navigator.of(context).pop(bookAppointmentDBModel);

                          }
                          _provider.isLoading = false;
                        }
                        else{
                          model.isLoading = false;
                        }
                      },
                    ),
                  ),
                ],
              ),
            ),); },),

      ),
    );
  }

  _titleText(String title){
    return  Padding(
      padding: const EdgeInsets.only(top: 20.0,left:20,right: 20),
      child: Text(title, style: HeadlineTextStyle,),
    );
  }
  _titleTextPassword(String title){
    return  Padding(
      padding: const EdgeInsets.only(left:20,right: 20),
      child: Text(title, style: HeadlineTextStyle,),
    );
  }
  _selectContactPerson(BookNewAppProvider model)
  {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0,horizontal: 20),
      child: DropdownButtonFormField(
        icon:  Image(
            image: AssetImage(
                ImagePath.bookAppointmentDropdownOrangeArrow)),
        iconEnabledColor: AppColors.colorOrange,
        hint: Text(
          model.selectedCPName==''? AppConstants.selectContactPersonText:model.selectedCPName,
          textAlign: TextAlign.center,
          style: userPwdTextStyle,
        ),
        //value: 1,//model.value,
        items: model.contactPersonList.map((BookAppAttendeeDBModel item) {
          return DropdownMenuItem<BookAppAttendeeDBModel>(
            child: Text(
              item.name,
              style: userPwdTextStyle,
            ),
            value: item,
          );
        }).toList(),
        onChanged: (value) {
          model.selectedContactPerson =  (value as BookAppAttendeeDBModel) ;
          print('${ model.selectedContactPerson!.name}');
          model.selectedCPName = model.selectedContactPerson!.name;

        },
        decoration: InputDecoration(
          hintText: '',
        ),
      ),
    );
  }

  _otherAttendeesTitle(){
    return Padding(
      padding: const EdgeInsets.only(top: 8.0,left: 20,right: 20),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text('Attendees',style: HeadlineTextStyle,),
          Container(
            margin: const EdgeInsets.only(left: 16.0),
            child: IconButton( onPressed: () {
              _alertMailController.text = '';
              _alertNameController.text = '';
              _alertAttContactController.text = '';
              _alertAddressController.text = '';
              addAttendeeAlert(context);
            }, icon: const Icon(Icons.add_circle, color: AppColors.colorOrange, size: 40,)),

          )

        ],
      ),
    );
  }
  addAttendeeAlert(BuildContext context)  {
    return showDialog<void>(
      barrierColor: Colors.white.withOpacity(0),
      useSafeArea: false,
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return Container(
          color: AppColors.colorGrey,
          height: double.infinity,
          width: double.infinity,
          child: AlertDialog(
            title:_titleAlert(AppConstants.addAttendeeAdd),
            content:  _alertContains(),
            actions: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Expanded(
                    flex: 1,
                    child:
                    InkWell(
                      onTap: ()=>   Navigator.of(context).pop(),
                      child:
                      TextButton(
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(4),
                            color: AppColors.colorBlack,
                          ),
                          margin: EdgeInsets.all(4),
                          padding: EdgeInsets.symmetric(vertical: 16,horizontal: 8),
                          height: 50,
                          width: 120,
                          child: Text(AppConstants.addAttendeeCancel, style: addAttendeeButton,
                            textAlign: TextAlign.center,
                          ),),
                        onPressed: (){
                          Navigator.pop(context);
                        },
                      ),),
                  ),
                  Expanded(
                    flex: 1,
                    child:
                    InkWell(
                      onTap: ()=>   Navigator.of(context).pop(),
                      child: TextButton(
                        child:  Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(4),
                            color: AppColors.colorOrange,
                          ),
                          margin: EdgeInsets.all(4),
                          padding: EdgeInsets.symmetric(vertical: 16,horizontal: 8),
                          height: 50,
                          child:Text(AppConstants.addAttendeeAdd, style: addAttendeeButton, textAlign: TextAlign.center,),),
                        onPressed: (){
                          if(_provider.selPartyType == 0){
                            CustomToast().customToast(AppConstants.partyTypeError);
                          }
                          else if (_formDialogKey.currentState!.validate()) {
                            print('else if entered');
                            _provider.addAttendeeToList(
                                _provider.selPartyType, documentId,  _alertNameController.text.toString(),
                                _alertMailController.text.toString(),
                                _alertAttContactController.text.toString(),
                                _alertAddressController.text.toString());
                            _provider.selPartyType = 0;
                            _provider.selPartyName = '';
                            _alertNameController.clear();
                            _alertMailController.clear();
                            _alertAttContactController.clear();
                            _alertAddressController.clear();
                            Navigator.pop(context);
                          }
                        }
                        ,),
                    ),
                  ),

                ],
              )
            ],),
        );
      },
    );
  }
  editAttendeeAlert(BuildContext context, int index, BookAppAttendeeDBModel bookAppAttendeeDBModel)  {

    return showDialog<void>(
      barrierColor: Colors.white.withOpacity(0),
      useSafeArea: false,
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return Container(
          color: AppColors.colorGrey,
          height: double.infinity,
          width: double.infinity,
          child: AlertDialog(
            title:_titleAlert(AppConstants.addAttendeeEdit),
            content:  _editAlertContains(bookAppAttendeeDBModel),
            actions: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Expanded(
                    flex: 1,
                    child:
                    InkWell(
                      onTap: ()=>   Navigator.of(context).pop(),
                      child:
                      TextButton(
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(4),
                            color: AppColors.colorBlack,
                          ),
                          margin: EdgeInsets.all(4),
                          padding: EdgeInsets.symmetric(vertical: 16,horizontal: 8),
                          height: 50,
                          width: 120,
                          child: Text(AppConstants.addAttendeeCancel, style: addAttendeeButton,
                            textAlign: TextAlign.center,
                          ),),
                        onPressed: (){
                          _provider.selPartyType = 0;
                          _provider.selPartyName = '';
                          Navigator.pop(context);
                        },
                      ),),
                  ),
                  Expanded(
                    flex: 1,
                    child:
                    InkWell(
                      onTap: ()=>   Navigator.of(context).pop(),
                      child: TextButton(
                        child:  Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(4),
                            color: AppColors.colorOrange,
                          ),
                          margin: EdgeInsets.all(4),
                          padding: EdgeInsets.symmetric(vertical: 16,horizontal: 8),
                          height: 50,
                          child:Text(AppConstants.addAttendeeSave, style: addAttendeeButton, textAlign: TextAlign.center,),),
                        onPressed: (){
                          if(_provider.selPartyType == 0){
                            CustomToast().customToast(AppConstants.partyTypeError);
                          }
                          else {
                            if (_formDialogKey.currentState!.validate()) {
                              _provider.editOtherAttendee(index, documentId, _provider.selPartyType,
                                  _alertNameController.text.toString(),
                                  _alertMailController.text.toString(),
                                  _alertAttContactController.text.toString(),
                                  _alertAddressController.text.toString());
                              _provider.selPartyType = 0;
                              _provider.selPartyName = '';
                              Navigator.pop(context);

                            }
                          }
                        },),
                    ),
                  ),

                ],
              )
            ],),
        );
      },
    );
  }
  _titleAlert(String title){
    return Container(
      child: Column(
        children: [
          Row(
            children: [
              Expanded(
                  flex: 6,
                  child: Text(title, style: addAttendeeButton.copyWith(color: AppColors.colorBlue,fontSize: 22),textAlign: TextAlign.center,)),
              Expanded(
                flex: 1,
                child: IconButton(
                  icon: Container(
                      width: 28,child: Image.asset(ImagePath.homeMenuClose)),
                  onPressed: () {
                    Navigator.of(context).pop(); // do something
                  },
                ),
              ),
            ],
          ),

          Divider(
            color: AppColors.colorGreyBorderLine,
            thickness: 1,
          )
        ],
      ),
    );
  }

  _alertContains(){
    return Container(
      // color: Colors.grey,
      height: 400,
      child: Form(
        key: _formDialogKey,
        child:
        SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              _titleTextAlert(AppConstants.selectPartyType),
              _partyTypeDropDown(_provider),
              _titleTextAlert(AppConstants.addAttendeeEnterName),
              _textFieldAlert(_alertNameController, 'Enter name', AppConstants.addAttendeeEnterName),
              _titleTextAlert(AppConstants.addAttendeeEnterEmail),
              _textFieldEmailAlert(),
              // _textFieldAlert(_alertMailController, 'enter email', AppConstants.addAttendeeEnterEmail),
              _titleTextAlert(AppConstants.addAttendeeContact),
              _textFieldContactAlert(_alertAttContactController, AppConstants.enterValidMobError, AppConstants.addAttendeeContact),

              _titleTextAlert(AppConstants.addAttendeeEnterAddress),
              _textFieldAddressAlert(_alertAddressController, 'Enter address', AppConstants.addAttendeeEnterAddress),
            ],
          ),
        ),


      ),
    );

  }
  _partyTypeDropDown(BookNewAppProvider model){
    return Padding(
      padding: const EdgeInsets.only(left: 8.0,right: 8,bottom: 8),
      child: DropdownButtonFormField(
        icon:  Image(
            image: AssetImage(
                ImagePath.bookAppointmentDropdownOrangeArrow)),
        iconEnabledColor: AppColors.colorOrange,
        hint: Text(
          model.selPartyName != '' ? model.selPartyName:
          AppConstants.selectPartyType,
          style: userPwdTextStyle,
        ),
        // value: citySelection,
        items: model.partyTypeList.map((PartyTypeSelection item) {
          return DropdownMenuItem<int>(
            child: Text(
              item.party_name,
              style:userPwdTextStyle,
            ),
            value: item.value,
          );
        }).toList(),
        onChanged: (value) {
          setState(() {
            int selVal =  value as int;
            print('selVal $selVal');
            model.selPartyName = model.partyTypeList[selVal].party_name;
            model.selPartyType = model.partyTypeList[selVal].party_type;
            print('party_type ${ model.selPartyType}');
          });


        },
        decoration: InputDecoration(
          hintText: '',

        ),
      ),
    );
  }

  _textFieldAlert(TextEditingController controller,String validatorMsg,String hintMsg){
    return Padding(
      padding: const EdgeInsets.only(left: 8.0,right: 8,bottom: 8),
      child: TextFormField(
        textInputAction: hintMsg==AppConstants.addAttendeeEnterAddress ? TextInputAction.done : TextInputAction.next,
        textCapitalization: TextCapitalization.values.first,
        validator: (value) {
          if (value!.isEmpty) {
            return validatorMsg;
          }
          return null;
        },
        decoration: InputDecoration(
          hintText: hintMsg,
          hintStyle: userPwdTextStyle,
        ),
        controller: controller,
        //  focusNode: hintMsg==AppConstants.addAttendeeEnterAddress?_addressFocusNode: null,
        maxLines: hintMsg==AppConstants.addAttendeeEnterAddress? 3 :  1,
        keyboardType:hintMsg==AppConstants.addAttendeeEnterAddress? TextInputType.multiline :  TextInputType.text,

      ),
    );
  }
  _textFieldContactAlert(TextEditingController controller,String validatorMsg,String hintMsg){
    return Padding(
      padding: const EdgeInsets.only(left: 8.0,right: 8,bottom: 8),
      child: TextFormField(
        textInputAction: TextInputAction.next,
        // onEditingComplete:() => FocusScope.of(context).requestFocus(focus),

        inputFormatters: [FilteringTextInputFormatter.digitsOnly],
        validator: (value) {
          if (value!.isEmpty || value.length!=10) {
            return validatorMsg;
          }
          return null;
        },
        decoration: InputDecoration(
          hintText: hintMsg,
          hintStyle: userPwdTextStyle,
        ),
        controller: controller,
        maxLines:   1,
        keyboardType: TextInputType.number,
        maxLength: 10,
      ),
    );
  }
  _textFieldAddressAlert(TextEditingController controller,String validatorMsg,String hintMsg){
    return Padding(
      padding: const EdgeInsets.only(left: 8.0,right: 8,bottom: 8),
      child:
      TextFormField(
        textCapitalization: TextCapitalization.values.first,
        textInputAction: TextInputAction.done ,
        validator: (value) {
          if (value!.isEmpty) {
            return validatorMsg;
          }
          return null;
        },
        decoration: InputDecoration(
          hintText: hintMsg,
          hintStyle: userPwdTextStyle,
        ),
        controller: controller,
        maxLines: 3,
        keyboardType:TextInputType.multiline ,

      ),
    );
  }
  _editAlertContains(BookAppAttendeeDBModel bookAppAttendeeDBModel){
    print('party_type ${bookAppAttendeeDBModel.partyType}');
    for(int i=0; i<_provider.partyTypeList.length;i++){
      if(_provider.partyTypeList[i].party_type == bookAppAttendeeDBModel.partyType){
        _provider.selPartyName = _provider.partyTypeList[i].party_name;
        _provider.selPartyType = bookAppAttendeeDBModel.partyType!;
      }
    }

    _alertNameController.text = bookAppAttendeeDBModel.name;
    _alertMailController.text = bookAppAttendeeDBModel.email;
    _alertAttContactController.text = bookAppAttendeeDBModel.contact;
    _alertAddressController.text = bookAppAttendeeDBModel.address!;
    return Container(
      // color: Colors.grey,
      height: 400,
      child: SingleChildScrollView(
        child:Form(
          key: _formDialogKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              _titleTextAlert(AppConstants.selectPartyType),
              _partyTypeDropDown(_provider),
              _titleTextAlert(AppConstants.addAttendeeEnterName),
              _textFieldAlert(_alertNameController, 'Enter name', AppConstants.addAttendeeEnterName),
              _titleTextAlert(AppConstants.addAttendeeEnterEmail),
              _textFieldEmailAlert(),

              _titleTextAlert(AppConstants.addAttendeeContact),
              _textFieldContactAlert(_alertAttContactController, AppConstants.enterValidMobError, AppConstants.addAttendeeContact),

              _titleTextAlert(AppConstants.addAttendeeEnterAddress),
              _textFieldAddressAlert(_alertAddressController, 'Enter address', AppConstants.addAttendeeEnterAddress),
            ],
          ),
        ),
      ),
    );
  }

  _textFieldEmailAlert(){
    return Padding(
      padding: const EdgeInsets.only(left: 8.0,right: 8,bottom: 8),
      child: TextFormField(
        textInputAction: TextInputAction.next,
        //autovalidateMode: AutovalidateMode.onUserInteraction,
        validator: (value) => EmailValidator.validate(value!) ? null : "Please enter a valid email",
        decoration: InputDecoration(
          hintText: AppConstants.addAttendeeEnterEmail,
          hintStyle: userPwdTextStyle,
        ),
        controller: _alertMailController,
        keyboardType:TextInputType.emailAddress,

      ),
    );
  }
  String? validateEmail(String? value) {
    String pattern =
        r"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]"
        r"{0,253}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]"
        r"{0,253}[a-zA-Z0-9])?)*$";
    RegExp regex = RegExp(pattern);
    if (value == null || value.isEmpty || !regex.hasMatch(value))
      return 'Enter a valid email address';
    else
      return null;
  }

  _addOtherAttendees(){
    return Container(
      padding: const EdgeInsets.only(left: 20.0,right: 20),
      // height: 50, width: double.infinity,
      child: Row(
        children: [
          Expanded(
            flex:3,
            child:  ListView.builder(
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                itemCount: _provider.addAttendeeList.length,
                itemBuilder: (context, index) {

                  return CheckboxListTile(
                    contentPadding: EdgeInsets.all(0),
                    controlAffinity: ListTileControlAffinity.leading,
                    activeColor: AppColors.colorBlue,
                    title: Row(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          _provider.addAttendeeList[index].name,
                          style: titleT2,
                        ),
                      ],
                    ),
                    value: true,
                    onChanged: (bool? value) {

                    },
                    secondary: Container(
                      width: 100,
                      child:
                      Row(
                        children: [
                          Transform.scale(
                            scale: 0.7,
                            child: IconButton(
                              onPressed: (){
                                print('index $index');

                                editAttendeeAlert( context,  index, new BookAppAttendeeDBModel(partyType: _provider.addAttendeeList[index].partyType, env: '', name:  _provider.addAttendeeList[index].name, email:  _provider.addAttendeeList[index].email, contact:  _provider.addAttendeeList[index].contact, address:  _provider.addAttendeeList[index].address));
                              },
                              icon: new Image.asset(ImagePath.bookAppointmentEdit),
                            ),
                          ),

                          Transform.scale(
                            scale: 0.7,
                            child: IconButton(
                              onPressed: (){
                                print('index $index');
                                _provider.deleteOtherAttendee(index, _provider.addAttendeeList[index].name);
                              },
                              icon: new Image.asset(ImagePath.getBiometricDelete),
                            ),
                          ),

                        ],
                      ),
                    ),
                  );
                }) ,
          ),

        ],
      ),
    );
  }
  _titleTextAlert(String title){
    return  Padding(
      padding: const EdgeInsets.only(right: 8,left: 8,top: 8),
      child: Text(title,style: title == AppConstants.aptmSelectAttendees ? HeadlineTextStyle.copyWith(color: AppColors.colorBlue):HeadlineTextStyle,),
    );
  }


  _addressDetails(BookNewAppProvider model){
    return
      Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          _titleText(AppConstants.buildingTypeText),
          _selectBuildingType(model),
          _titleText(AppConstants.addressTypeText),
          _selectAddressType(model),

          if(model.showDiffAddress)
            Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  _titleText(AppConstants.addressLine1Text),
                  _textField(_buildingNameController, AppConstants.buildingNoValidText, AppConstants.buildingNoText),
                  _titleText(AppConstants.addressLine2Text),
                  _textField(_cityStateNameController, AppConstants.cityProvValidText, AppConstants.cityProvienceText),
                ],
              ),),

          _titleText(AppConstants.addressLine3Text),
          _textFieldLand(_landmarkController, AppConstants.landmarkValidText, AppConstants.landmarkText)
        ],
      );
  }
  _selectBuildingType(BookNewAppProvider model){
    return Padding(
      padding: const EdgeInsets.only(right: 8.0),
      child: Row(
        children: [
          Expanded(
            flex: 1,
            child: CustomRadioButtonListTile(
              radioBtnName: AppConstants.apartmentText,
              value: 1,
              groupValue: model.groupBuildingType,
              onChanged: (int? newValue) {
                setState(() {
                  print(newValue);
                  model.groupBuildingType= newValue!;
                });
              },
            ),
          ), Expanded(
            flex: 1,
            child: CustomRadioButtonListTile(
              radioBtnName: AppConstants.individualHouseText,
              value: 2,
              groupValue: model.groupBuildingType,
              onChanged: (int? newValue) {
                setState(() {
                  print(newValue);
                  model.groupBuildingType= newValue!;
                });

              },
            ),
          ),

        ],
      ),
    );
  }

  _selectAddressType(BookNewAppProvider model){
    return Column(
      children: [
        CustomRadioButtonListTile(
          radioBtnName: AppConstants.sameAddressText,
          value: 1,
          groupValue: model.groupAddressType,
          onChanged: (int? newValue) {
            model.groupAddressType= newValue!;
            model.showDiffAddress = false;
          },
        ), CustomRadioButtonListTile(
          radioBtnName: AppConstants.differentAddressText,
          value: 2,
          groupValue: model.groupAddressType,
          onChanged: (int? newValue) {
            model.groupAddressType= newValue!;
            model.showDiffAddress = true;
          },
        ),

      ],
    );
  }
  _textField(TextEditingController controller,String validatorMsg, String hintMsg){
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0,horizontal: 20),
      child: TextFormField(
        textInputAction: TextInputAction.next,
        textCapitalization: TextCapitalization.values.first,
        autovalidateMode:
        AutovalidateMode.onUserInteraction,
        validator: (value) {
          if (value!.isEmpty) {
            return validatorMsg;
          }
          return null;
        },
        decoration: InputDecoration(
          hintText: hintMsg,
          // hintStyle: userPwdTextStyle,
        ),
        controller: controller,
        keyboardType: TextInputType.text,

      ),
    );
  }
  _textFieldLand(TextEditingController controller,String validatorMsg, String hintMsg){
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0,horizontal: 20),
      child: TextFormField(
        textInputAction: TextInputAction.done,
        textCapitalization: TextCapitalization.values.first,
        autovalidateMode:
        AutovalidateMode.onUserInteraction,
        validator: (value) {
          if (value!.isEmpty) {
            return validatorMsg;
          }
          return null;
        },
        decoration: InputDecoration(
          hintText: hintMsg,
        ),
        controller: controller,
        keyboardType: TextInputType.text,

      ),
    );
  }

  _billingInformationDetails(BookNewAppProvider model){
    return Container(
      height: 173,
      width: 360,
      margin: EdgeInsets.fromLTRB(24,24,24,30),
      decoration: BoxDecoration(
        color: AppColors.colorGreen,
        borderRadius:
        BorderRadius.all(Radius.circular(12.0)),

      ),
      child: Column(
        children: [
          Container(
            height: 132,
            width: 360,
            // margin: EdgeInsets.fromLTRB(24,24,24,0),
            padding: EdgeInsets.only(left: 16,right: 16),
            decoration: BoxDecoration(
              color: AppColors.colorGrey,
              borderRadius:
              BorderRadius.all(Radius.circular(12.0)),

            ),
            child: SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Row(

                    children: <Widget>[

                      SizedBox(
                          height: 45,
                          child: Center(child: Text(AppConstants.billingInfoText, style: billingInfoTextStyle,textAlign: TextAlign.center,))),
                      /*  Expanded(
                        flex:1,
                        child: IconButton(icon: const Icon(Icons.keyboard_arrow_up,), //keyboard_arrow_down_outlined
                          onPressed:() {

                          },),),*/
                    ],
                  ),

                  Divider(
                    color: Colors.grey,
                  ),
                  SizedBox(
                    height: 19,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(AppConstants.appChargesText,
                        style: ApmtDetailsLabel,),
                      Text(model.appointmentCharges.toString()+ AppConstants.inrText,
                        style: billingInfoTextStyle,),
                    ],
                  ),

                ],
              ),
            ),

          ),
          Container(
            margin: EdgeInsets.only(left: 24, right: 24,top: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(AppConstants.appCreditBalance,
                  style: appCreditBalLabel,),
                Text(model.creditBalance.toString().replaceAll(regex, "")+ AppConstants.inrText,
                  style: appBalWhiteTextStyle,),
              ],
            ),
          ),
        ],
      ),

    );
  }
 /* checkUserExists(BuildContext context)  {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text(AppConstants.existingUserText),
          content:  Text(MsgConstants.checkUserLogin),
          actions: <Widget>[
            TextButton(
              child:  const Text(AppConstants.noText),
              onPressed: () {
                Navigator.push(
                    context, MaterialPageRoute(builder: (context) => EnrollPartnerView(status: '',  mobile: '', email: '')));
              },
            ),
            TextButton(
              child:  const Text(AppConstants.yesText),
              onPressed: () async {
                Navigator.push(context, MaterialPageRoute(builder: (context) => LoginView()));
              },
            ),
          ],
        );
      },
    );
  }*/
  void getChargesDetails() async{
    String? email = await SharedPreferenceUtils.getStringPreference(
        SharedPreferenceUtils.EMAIL_ID);


    ChargesDetailsResponseModel? response = await _provider.getChargesDetails(selectedCity, selectedRegion);
    print('response ${response!.appointmentCharges}');
    setState(() {
      _provider.appointmentCharges = response.appointmentCharges!;

      if(email != null ) {
        _provider.creditBalance = response.creditBalance!.toDouble();
        if (_provider.creditBalance >= _provider.appointmentCharges) {
          _provider.amountToPay = 0;
        }
        else if (_provider.creditBalance < _provider.appointmentCharges &&
            _provider.creditBalance > 0) {
          _provider.amountToPay = (_provider.appointmentCharges -
              _provider.creditBalance.toInt()) as int;
        }
        else if (_provider.creditBalance == 0) {
          _provider.amountToPay = _provider.appointmentCharges;
        }
        // }

        print("amount to pay is for get charges details is ${_provider.amountToPay}");

      }});

  }
  showCustomDialog()async{
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile || connectivityResult == ConnectivityResult.wifi) {

      showDialog(context: context,
          builder: (BuildContext context){
            return Container(
              color: AppColors.colorGrey,
              height: double.infinity,
              width: double.infinity,
              child: CustomDialogBox(
                citySelection: _provider.navigationModel.citySelection ,
                regionSelection: _provider.navigationModel.regionSelection ,
                selectedDate: _provider.navigationModel.selectedDate,
                timeSlotValue: _provider.navigationModel.timeSlotValue,
                selectedTimeSlot: _provider.navigationModel.selectedTimeSlot,
                selectedDateInDateFormat: _provider.navigationModel.selectedDateInDateFormat,
                divisionId: _provider.navigationModel.divisionId,
                regionList : _provider.navigationModel.regionList,
                timeSlotList: _provider.navigationModel.timeSlotList,
                slotId: _provider.navigationModel.slotId,
                token: _tokenNoController.text,
                password: _passwordController.text,
                attendeeList: _provider.addAttendeeList,
                contactPersonList: _provider.contactPersonList,
                selectedCP: _provider.selectedCPName,
                buildingType: _provider.groupBuildingType,
                addressType: _provider.groupAddressType,
                buildingName: _buildingNameController.text,
                cityState: _cityStateNameController.text,
                landmark: _landmarkController.text,
                navigationPage: _provider.navigationModel.navtigationPage,
                callbackFunction:callback,
              ), ); });
    } else {
      CustomToast().customToast(AppConstants.pleaseCheckYourInternetConnection);
    }


  }
   callback(NavigationModel navigationModel){
    print('call back is called ${navigationModel.citySelection} ${navigationModel.regionSelection} ${navigationModel.selectedTimeSlot}');
    selectedRegion = navigationModel.regionSelection;
    selectedCity = navigationModel.citySelection;
    setState(() {

      _dateController.text = navigationModel.selectedDate+ ' '+navigationModel.timeSlotValue;
      _cityController.text = navigationModel.citySelection;
      _regionController.text = navigationModel.regionSelection;
      _tokenNoController.text= navigationModel.token;
      _passwordController.text = navigationModel.password;
      _provider.addAttendeeList.addAll(navigationModel.attendeeList);
      _provider.contactPersonList.addAll(navigationModel.contactPersonList);
      _provider.selectedCPName = navigationModel.selectedCP;
      _provider.groupBuildingType = navigationModel.buildingType;
      _provider.groupAddressType = navigationModel.addressType;
      _buildingNameController.text = navigationModel.buildingName;
      _cityStateNameController.text = navigationModel.cityState;
      _landmarkController.text = navigationModel.landmark;
      _provider.appDate = navigationModel.selectedDate+ ' '+navigationModel.timeSlotValue;
      _provider.navigationModel = navigationModel;
    });
  }
  static Future<T?> pushNamedAndRemoveUntil<T extends Object?>(
      BuildContext context,
      String newRouteName,
      RoutePredicate predicate, {
        Object? arguments,
      }) {
    print('arguments $arguments');
    return Navigator.of(context).pushNamedAndRemoveUntil<T>(newRouteName, predicate, arguments: arguments);
  }


  Future<bool> _onBackPressed() {
    DateTime now = DateTime.now();
    if (currentBackPressTime == null ||
        now.difference(currentBackPressTime!) > const Duration(seconds: 2)) {
      currentBackPressTime = now;
      Fluttertoast.showToast(
          msg: 'Please click BACK again to exit',
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM);
      return Future.value(false);
    }
    exit(0);

  }


}
