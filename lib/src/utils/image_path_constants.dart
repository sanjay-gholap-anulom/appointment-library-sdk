class ImagePath{
  static const String splashScreenImage = 'Assets/images/splash_screen.png';
  static const String logoImage = 'Assets/images/logo_anulom_ramm.png';
  static const String userNameIcon = 'Assets/images/usernameIcon.png';
  static const String passwordLockIcon = 'Assets/images/passwordLockIcon.png';
  static const String passwordShowIcon = 'Assets/images/passwordShowIcon.png';
  static const String passwordHideIcon = 'Assets/images/passwordHideIcon.png';
  static const String emailTextIcon = 'Assets/images/emailTextIcon.png';
  static const String emailSentGreen = 'Assets/images/emailSentGreen.png';
  static const String backButtonForgotPassword = 'Assets/images/backArrow.png';
  static const String bookAppointmentCalender = 'Assets/images/calendarIconOrange.png';
  static const String homePageCalender   = 'Assets/images/calenderIcon.png';

  static const String drawerBackgroundImage = 'Assets/images/background.png';
  static const String drawerIcon = 'Assets/images/drawerIcon.png';
  static const String commonLeftArrow = 'Assets/images/leftArrowIcon.png';

  static const String drawerMenuIcon1 = 'Assets/images/addAppoinmentIcon.png';
  static const String drawerMenuIcon2 = 'Assets/images/paymentHistory.png';
  static const String drawerMenuIcon3 = 'Assets/images/cancelledAptsMenu.png';
  static const String drawerMenuIcon4 = 'Assets/images/doneAptsMenu.png';
  static const String drawerMenuIcon6 = 'Assets/images/logout.png';

  static const String drawerMenuIconActive1 = 'Assets/images/addAppointmentActive.png';
  static const String drawerMenuIconActive2 = 'Assets/images/payHistoryActive.png';
  static const String drawerMenuIconActive3 = 'Assets/images/cancelledAptsActive.png';
  static const String drawerMenuIconActive4 = 'Assets/images/doneAptsActive.png';
  static const String drawerMenuIconActive6 = 'Assets/images/logoutActive.png';
  static const String iconHome= 'Assets/images/iconHome.png';
  static const String iconHomeActive= 'Assets/images/homeActive.png';
  static const String homePageMenu = 'Assets/images/menuIcon.png';
  static const String homePageSearch = 'Assets/images/searchIcon.png';
  static const String blankProfileDrawer = 'Assets/images/profileDummyImage.png';
  static const String leftArrowIcon = 'Assets/images/leftArrowIcon.png';
  static const String callIcon = "Assets/images/call.png";
  static const String bookAppointmentDropdownOrangeArrow = 'Assets/images/dropDownArrowOrange.png';
  static const String amountIcon = 'Assets/images/iconAmount.png';
  static const String homeCancel = 'Assets/images/homeCancel.png';
  static const String homeCreateIcon = 'Assets/images/homeCreateIcon.png';
  static const String homeDelete = 'Assets/images/homeDelete.png';
  static const String homePaid = 'Assets/images/homePaid.png';
  static const String homeReschedule = 'Assets/images/homeReschedule.png';
  static const String rescheduleTimeIcon = 'Assets/images/rescheculeDate.png';
  static const String bookAppointmentEdit = 'Assets/images/editIConBlue.png';
  static const String getBiometricDelete = 'Assets/images/deleteIcon.png';
  static const String homeMenuClose   = 'Assets/images/menuCloseIcon.png';
  static const String splashScreenNewImage = 'Assets/images/splashLogoLight.png';

}