import 'package:flutter/material.dart';

class AppColors {
  static const Color colorOrange = Color(0xFFF14336);
  static const Color colorDarkGreen = Color(0xFF00863E);
  static const Color colorGreen = Color(0xFF2FCC84);
  static const Color colorWhite = Color(0xFFFFFFFF);
  static const Color colorBlack = Color(0xFF000000);
  static const Color colorBlue = Color(0xFF285296);
  static const Color colorHintText = Color(0xFF333333);
  static const Color colorGrey = Color(0xFFEEEEEE);
  static const Color colorGreyBorderLine = Color(0xFFD1D1D1);
  static const Color colorGreyTaskText = Color(0xFF888888);
  static const Color colorDarkBlue = Color(0xFF131C40);
  static const Color colorDarkGreyDetails = Color(0xFF333333);
  static const Color colorGreyDrawer = Color(0xFFBFBFBF);
  static const Color colorLightGreyDetails = Color(0xFF555555);
  static const Color containerGrey = Color(0xFFF5F5F5);
  static const Color colorSkyBlueText = Color(0xFF004CC8);
  static const Color shadowColor = Color(0xFFE5E5E5);
  static const Color availBalanceColor = Color(0xFFF5F5F5);
  static const Color tealRescheduleColor = Color(0xFF007696);
  static const Color dividerGreyColor = Color(0xFFCCCCCC);
  static const Color crossGreyColor = Color(0xFFAAAAAA);
  static const Color blueGreyColor = Color(0xFF3E526E);
  static const Color colorLightBlue = Color(0xFFECF4FF);
}