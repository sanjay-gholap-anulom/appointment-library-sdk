
import 'dart:io';

import 'package:shared_preferences/shared_preferences.dart';

class SharedPreferenceUtils {
  static SharedPreferences? preferences;
  static const AUTH_TOKEN = 'auth_key';
  static const EMAIL_ID = 'emailId';
  static const VERSION='version';
  static const PASSWORD = 'password';
  static const USER_NAME = 'user_name';
  static const MERCHANT_NAME = 'merchant_name';
  static const PLATFORM_ID = 'platform_id';
  static const staticToken = 'DOtUBMhv5pk51tl0D37uBcezq85cXNN7hZQ7';
  static const DOWNLOAD_TIME_APPOINTMENT = "download_time_appointment";
  static const PAYNOWBUTTONSHOWFLAG = "paynow_button_show_flag";
  static const MOBILENUMBER = "contact_number";
  static const IMPORT_FLAG='import_flag';
  static const IMPORT_USERNAME='import_username';

  static Future<String?> getStringPreference(String key) async{
    final SharedPreferences  sharedPreferences = await SharedPreferences.getInstance();
    var str = sharedPreferences.getString(key);
    return str;
  }

  static Future<int?> getIntPreferences(String key) async{
    final SharedPreferences  sharedPreferences = await SharedPreferences.getInstance();
    var str=sharedPreferences.getInt(key);
    return str;
  }

  static Future<void> setStringPreference(String key, String value)async{
    final SharedPreferences  sharedPreferences = await SharedPreferences.getInstance();
    var str = sharedPreferences.setString(key, value);
  }
  static void setIntPreference(String key, int value)async{
    final SharedPreferences  sharedPreferences = await SharedPreferences.getInstance();
    var str = sharedPreferences.setInt(key,value);
  }



  static Future<void> setValuesToPreferences(String email, String password, String? version, String? userName, String? merchantName, String? token, int? platformId, String contactNumber) async{
    // await SharedPreferences.getInstance().setString(AUTH_TOKEN, );
    final SharedPreferences  sharedPreferences = await SharedPreferences.getInstance();
    const end1 = "@";
    final endIndex = email.indexOf(end1);
    print(email.substring(0 , endIndex));
    var userNameEmail = email.substring(0 , endIndex);
    if(userNameEmail.contains('.')){
      const end2 = ".";
      final endIndex2 = email.indexOf(end2);
      userName = userNameEmail.substring(0,endIndex2);
    }
     sharedPreferences.setString(EMAIL_ID, email);
     sharedPreferences.setString(VERSION, version!);
     sharedPreferences.setString(PASSWORD, password);
     sharedPreferences.setString(USER_NAME, userName?? '' ); //?? userNameEmail
    sharedPreferences.setString(MERCHANT_NAME, merchantName!);
     sharedPreferences.setString(AUTH_TOKEN, staticToken);
     sharedPreferences.setInt(PLATFORM_ID, platformId!);
     sharedPreferences.setInt(PAYNOWBUTTONSHOWFLAG, -1);
     sharedPreferences.setString(MOBILENUMBER, contactNumber);

  }
  static Future<void> setImportFlag(String importFlag) async{
    // await SharedPreferences.getInstance().setString(AUTH_TOKEN, );
    final SharedPreferences  sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setString(IMPORT_FLAG, importFlag);
  }
  static Future<void> setImportUserName(String userName) async{
    // await SharedPreferences.getInstance().setString(AUTH_TOKEN, );
    final SharedPreferences  sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setString(IMPORT_USERNAME, userName);
  }

}
