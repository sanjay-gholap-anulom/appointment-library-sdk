import 'package:flutter/material.dart';

import 'color_constants.dart';

const TextStyle titleT1 = TextStyle(
    fontFamily: "Roboto",
    fontWeight: FontWeight.w600,
    fontSize: 16,
   );

const TextStyle titleT4 = TextStyle(
  fontFamily: "Roboto",
  fontWeight: FontWeight.w700,
  fontSize: 20,
  color: AppColors.colorBlack
);

const TextStyle titleT2 = TextStyle(
  fontFamily: "Roboto",
  fontWeight: FontWeight.w400,
  fontSize: 15,
  color: AppColors.colorBlack,
);

const TextStyle titleT3 = TextStyle(
  fontFamily: "Roboto",
  fontWeight: FontWeight.w400,
  fontSize: 16,
  color: AppColors.colorOrange,
);

const TextStyle titleHeadline = TextStyle(
  fontFamily: "Roboto",
  fontWeight: FontWeight.w600,
  fontSize: 18,
  color: AppColors.colorDarkBlue,
);

const TextStyle tabViewStyle = TextStyle(
  fontFamily: "Roboto",
  fontWeight: FontWeight.w700,
  fontSize: 13,
  color: AppColors.colorGreyTaskText,

);

const TextStyle dropdownStyle = TextStyle(
  fontFamily: "Roboto",
  fontWeight: FontWeight.w600,
  fontSize: 16,
  color: AppColors.colorWhite,
);


const TextStyle userPwdTextStyle = TextStyle(
  fontFamily: "Roboto",
  fontWeight: FontWeight.w400,
  fontSize: 16,
  color: AppColors.colorBlack,
);
const TextStyle loginBtnTextStyle = TextStyle(
  fontFamily: "Roboto",
  fontWeight: FontWeight.w600,
  fontSize: 20,
  color: AppColors.colorWhite,

);
const TextStyle forgotPwdTextStyle = TextStyle(
  fontFamily: "Roboto",
  fontWeight: FontWeight.w400,
  fontSize: 14,
  color: AppColors.colorBlue,
);
const TextStyle loginPageLabelTextStyle = TextStyle(
  fontFamily: "Roboto",
  fontWeight: FontWeight.normal,
  fontSize: 14,
 // height: 0.5,
  color: AppColors.colorBlack,
);
const TextStyle contentTextStyle = TextStyle(
  fontFamily: "Roboto",
  fontWeight: FontWeight.normal,
  fontSize: 16,
  color: AppColors.colorHintText,

);

const TextStyle inSyncTextStyle = TextStyle(
  fontFamily: "Roboto",
  fontWeight: FontWeight.w600,
  fontSize: 10,
  color: AppColors.colorGreen,
);

const TextStyle labelWhiteText = TextStyle(
  fontFamily: "Roboto",
  fontWeight: FontWeight.w600,
  fontSize: 12,
  color: AppColors.colorWhite,
);

const TextStyle complaintAppBarTextStyle = TextStyle(
  fontFamily: "Roboto",
  fontWeight: FontWeight.w600,
  fontSize: 17,
  color: AppColors.colorDarkBlue,
);
const TextStyle radioBtnTextStyle = TextStyle(
  fontFamily: "Roboto",
  fontWeight: FontWeight.w400,
  fontSize: 14,
  color: AppColors.colorBlack,
);
const TextStyle HeadlineTextStyle = TextStyle(
  fontFamily: "Roboto",
  fontWeight: FontWeight.w600,
  fontSize: 14,
  color: AppColors.colorBlack,
);
const TextStyle HeadlineWhiteTextStyle = TextStyle(
  fontFamily: "Roboto",
  fontWeight: FontWeight.w600,
  fontSize: 14,
  color: AppColors.colorWhite,
);
const TextStyle ApmtDetailsLabel = TextStyle(
  fontFamily: "Roboto",
  fontWeight: FontWeight.w400,
  fontSize: 14,
  color: AppColors.colorBlack,
);
const TextStyle ApmtDetailsLabelTitleBlue = TextStyle(
  fontFamily: "Roboto",
  fontWeight: FontWeight.w600,
  fontSize: 14,
  color: AppColors.colorBlue,
);
const TextStyle ApmtDetailsLabelOtherColor = TextStyle(
  fontFamily: "Roboto",
  fontWeight: FontWeight.w400,
  fontSize: 12,
  color: AppColors.colorLightGreyDetails,
);
const TextStyle boldTextStyle = TextStyle(
  fontFamily: "Roboto",
  fontWeight: FontWeight.w600,
  fontSize: 14,
  color: AppColors.colorBlack,
);
const TextStyle TitleBlueTextStyle = TextStyle(
  fontFamily: "Roboto",
  fontWeight: FontWeight.w400,
  fontSize: 13,
  color: AppColors.colorSkyBlueText,
);
const TextStyle subtitleTextStyle = TextStyle(
  fontFamily: "Roboto",
  fontWeight: FontWeight.w600,
  fontSize: 14,
  color: AppColors.colorBlack,
);

const TextStyle addAttendeeButton = TextStyle(
  fontFamily: "Roboto",
  fontWeight: FontWeight.w700,
  fontSize: 16,
  color: AppColors.colorWhite,
);

const TextStyle emailSentTextStyle = TextStyle(
  fontFamily: "Roboto",
  fontWeight: FontWeight.w300,
  fontSize: 22,
  color: AppColors.colorBlack,
);
const TextStyle enrollTitleTextStyle = TextStyle(
  fontFamily: "Roboto",
  fontWeight: FontWeight.w700,
  fontSize: 24,
  color: AppColors.colorBlack,
);
const TextStyle phoneNoTextStyle = TextStyle(
  fontFamily: "Roboto",
  fontWeight: FontWeight.w500,
  fontSize: 22,
  color: AppColors.colorBlack,
);
const TextStyle phoneNoLabelStyle = TextStyle(
  fontFamily: "Roboto",
  fontWeight: FontWeight.w600,
  fontSize: 16,
  color: AppColors.colorBlack,
);
const TextStyle resendOtpStyle = TextStyle(
  fontFamily: "Roboto",
  fontWeight: FontWeight.w500,
  fontSize: 14,
  color: AppColors.colorBlue,
);

const TextStyle enrollFormLabelStyle = TextStyle(
  fontFamily: "Roboto",
  fontWeight: FontWeight.w500,
  fontSize: 14,
  color: AppColors.colorBlack,
);
const TextStyle addAmountBtnStyle = TextStyle(
  fontFamily: "Roboto",
  fontWeight: FontWeight.w700,
  fontSize: 13,
  color: AppColors.colorWhite,
);
const TextStyle availBalTextStyle = TextStyle(
  fontFamily: "Roboto",
  fontWeight: FontWeight.w600,
  fontSize: 14,
  color: AppColors.colorBlack,
);
const TextStyle debAmountTextStyle = TextStyle(
  fontFamily: "Roboto",
  fontWeight: FontWeight.w600,
  fontSize: 16,
  color: AppColors.colorBlack,
);
const TextStyle remarkTextStyle = TextStyle(
  fontFamily: "Roboto",
  fontWeight: FontWeight.w700,
  fontSize: 14,
  color: AppColors.colorBlack,
);
const TextStyle billingInfoTextStyle = TextStyle(
  fontFamily: "Roboto",
  fontWeight: FontWeight.bold,
  fontSize: 16,
  color: AppColors.colorBlack,

);
const TextStyle appCreditBalLabel = TextStyle(
  fontFamily: "Roboto",
  fontWeight: FontWeight.w400,
  fontSize: 14,
  color: AppColors.colorWhite,
);
const TextStyle appBalWhiteTextStyle = TextStyle(
  fontFamily: "Roboto",
  fontWeight: FontWeight.bold,
  fontSize: 16,
  color: AppColors.colorWhite,

);