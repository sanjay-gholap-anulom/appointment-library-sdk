
class Api {
  static const String BASE_URL = 'http://dev.anulom.com:3003/api/';//http://dev.anulom.com:3000/api/v4/appointment_data/login
  static const String LOGIN_URL = BASE_URL + 'v2/document/login';
  static const String CANCEL_APPT_URL = BASE_URL + 'v2/document/cancel_appointment';
  static const String RESCHEDULE_APPT_URL = BASE_URL + 'v2/document/reschedule_appointment';

  static const String FORGOT_PASSWORD = 'http://dev.anulom.com:3003/api/v2/document/forgot_password';
  //Sign Up
  static const String GET_OTP = 'http://dev.anulom.com:3003/api/v2/document/get_otp';
  static const String PARTNER_SIGNUP = 'http://dev.anulom.com:3003/api/v2/document/create_partner';

  //Available Balance
  static const String CHARGES_DETAILS = 'http://dev.anulom.com:3003/api/v2/document/get_charges_details';

  //Book new Appointment
  static const String GET_BIOMETRIC =  'http://dev.anulom.com:3000/api/v3/appointment_data/witness_biometric';
  static const String GET_CITY = 'http://dev.anulom.com/plus/api/v1/appointment_data/get_city';
  static const String GET_REGIONS = 'http://dev.anulom.com/plus/api/v1/appointment_data/get_regions';
  static const String GET_TIME_SLOT = 'http://dev.anulom.com/plus/api/v1/get_time_slot';
  static const String  BOOK_APPOINTMENT_URL = 'http://dev.anulom.com:3003/api/v2/document/book_appointment';
  static const String  GET_BOOK_APPOINTMENT_URL = 'http://dev.anulom.com:3003/api/v2/document/get_appointments';

  //Payment History
  static const String CREDIT_HISTORY = 'http://dev.anulom.com:3003/api/v2/document/get_credit_history';
  static const String DEBIT_HISTORY = 'http://dev.anulom.com:3003/api/v2/document/get_debit_history';
 static const String IMPORT_DB_URL = 'https://s3-ap-south-1.amazonaws.com/riderapp-db-dump/test/';
  static const String PAYMENT_DETAILS = 'http://dev.anulom.com:3003/api/v2/document/update_payment_details';

}





/*
class Api {
  static const String BASE_URL = 'https://ramm.digitalrenting.com/api/';//http://dev.anulom.com:3000/api/v4/appointment_data/login
  static const String LOGIN_URL = BASE_URL + 'v2/document/login';
  static const String CANCEL_APPT_URL = BASE_URL + 'v2/document/cancel_appointment';
  static const String RESCHEDULE_APPT_URL = BASE_URL + 'v2/document/reschedule_appointment';

  static const String FORGOT_PASSWORD = BASE_URL + 'v2/document/forgot_password';

  static const String GET_OTP = 'https://ramm.digitalrenting.com/api/v2/document/get_otp';
  static const String PARTNER_SIGNUP =  BASE_URL + 'v2/document/create_partner';

  //Available Balance
  static const String CHARGES_DETAILS = BASE_URL+ 'v2/document/get_charges_details';

  //Book new Appointment
 // static const String GET_BIOMETRIC =  'http://dev.anulom.com:3000/api/v3/appointment_data/witness_biometric';
  static const String GET_CITY = 'https://anulom.com/plus/api/v1/appointment_data/get_city';
  static const String GET_REGIONS = 'https://anulom.com/plus/api/v1/appointment_data/get_regions';
  static const String GET_TIME_SLOT = 'https://anulom.com/plus/api/v1/get_time_slot';
  static const String  BOOK_APPOINTMENT_URL = 'https://ramm.digitalrenting.com/api/v2/document/book_appointment';
  static const String  GET_BOOK_APPOINTMENT_URL = 'https://ramm.digitalrenting.com/api/v2/document/get_appointments';

  //Payment History
  static const String CREDIT_HISTORY = 'https://ramm.digitalrenting.com/api/v2/document/get_credit_history';
  static const String DEBIT_HISTORY = 'https://ramm.digitalrenting.com/api/v2/document/get_debit_history';
  static const String IMPORT_DB_URL = 'https://s3-ap-south-1.amazonaws.com/riderapp-db-dump/test/';

  static const String PAYMENT_DETAILS = 'https://ramm.digitalrenting.com/api/v2/document/update_payment_details';

}

*/
