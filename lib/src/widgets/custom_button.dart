
import 'package:flutter/material.dart';

import '../utils/color_constants.dart';
import '../utils/text_styles.dart';

class CustomButton extends StatelessWidget {

  final String? buttonName;
  final VoidCallback? onPressed;
  final double? minWidth;
  final double? height;
  const CustomButton({
    Key? key,
    @required this.buttonName,
    @required this.onPressed,
    @required this.minWidth,
    @required this.height,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ButtonTheme(
        child:ConstrainedBox(
          constraints: BoxConstraints.tightFor(
            width: minWidth ?? 330.0,
            height: height ?? 58.0,),
          child: ElevatedButton(
          style: ElevatedButton.styleFrom(primary: AppColors.colorOrange,),
           child: Text('$buttonName', style: loginBtnTextStyle,
           ),
          onPressed: onPressed,
          ),
        ),
    );
  }
}
