import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CustomRadioButtonListTile extends StatelessWidget {
  const CustomRadioButtonListTile({
    Key? key,
    required this.radioBtnName,
    required this.groupValue,
    required this.value,
    required this.onChanged,
  }) : super(key: key);

  final String radioBtnName;
  final int groupValue;
  final int value;
  final ValueChanged<int> onChanged;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 40,
      child: RadioListTile(
          activeColor: Colors.blue,
         // tileColor: MaterialStateColor.resolveWith((states) => AppColors.colorBlue),
          title: Text('$radioBtnName', //style: radioBtnTextStyle,
          ),
          groupValue: groupValue,
          value: value,
          onChanged: (int? newValue) {
            onChanged(newValue!);
            print(newValue);
          }),
    );
  }
}
