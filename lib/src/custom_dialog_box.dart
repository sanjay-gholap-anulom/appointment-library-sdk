import 'dart:convert';

import 'package:book_appointment_package/src/utils/app_constants.dart';
import 'package:book_appointment_package/src/utils/color_constants.dart';
import 'package:book_appointment_package/src/utils/image_path_constants.dart';
import 'package:book_appointment_package/src/utils/text_styles.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:modal_progress_hud_nsn/modal_progress_hud_nsn.dart';
import 'package:provider/provider.dart';
import 'package:intl/intl.dart';

import '../book_appointment_package.dart';
import 'book_new_appoint_provider.dart';
import 'custom_dialog_box_provider.dart';
class CustomDialogBox extends StatefulWidget {


  final citySelection;
  final regionSelection;
  final selectedDate;
  final  timeSlotValue;
  final selectedTimeSlot;
  final selectedDateInDateFormat;
  final divisionId;
  final regionList;
  final timeSlotList;
  final slotId;
  final token;
  final password;
  final attendeeList;
  final contactPersonList;
  final selectedCP;
  final buildingType;
  final addressType;
  final buildingName;
  final cityState;
  final landmark;
  final navigationPage;
  final Function callbackFunction;
  const CustomDialogBox({ required this.citySelection, required this.regionSelection, required this.selectedDate, required this.timeSlotValue, required this.selectedTimeSlot,
    required this.selectedDateInDateFormat, required this.divisionId, required this.regionList, required this.timeSlotList, required this.slotId, required this.token, required this.password,
    required this.attendeeList, required this.contactPersonList, required this.selectedCP, required this.buildingType, required this.addressType, required this.buildingName,
    required this.cityState, required this.landmark,required this.navigationPage, required this.callbackFunction}) : super();

  @override
  _CustomDialogBoxState createState() => _CustomDialogBoxState();
}

class _CustomDialogBoxState extends State<CustomDialogBox> {
  int itemWidth = 91;
  int itemHeight = 38;
  late String citySelection ;
  late String regionSelection;
  late String selectedDate;
  late int selectedTimeSlot;
  late DateTime selectedDateInDateFormat;
  String documentId = 'A_94403';
  CustomDialogBoxProvider _provider = CustomDialogBoxProvider();
  String timeSlotValue = '' ;

  @override
  void initState() {

    citySelection = widget.citySelection;
    regionSelection = widget.regionSelection;
    timeSlotValue = widget.timeSlotValue;
    selectedDate = widget.selectedDate;
    selectedTimeSlot = widget.selectedTimeSlot;
    selectedDateInDateFormat = widget.selectedDateInDateFormat;
    _provider.selDivisionId = widget.divisionId;
    _provider.cityAreaSelection.addAll(widget.regionList);
    _provider.timeSlotSelection.addAll(widget.timeSlotList);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      elevation: 0,
      backgroundColor: Colors.transparent,
      child: contentBox(context),
    );
  }
  contentBox(context){
    return ChangeNotifierProvider<CustomDialogBoxProvider>(
      create: (context) => _provider,
      child: WillPopScope(
        onWillPop: () => navigateToBookAppPage(context),
        child: Consumer<CustomDialogBoxProvider>(builder: (context, model, child) {
          return ModalProgressHUD(
              inAsyncCall: model.isLoading,
            child:  GestureDetector(
              onTap: () {
                FocusScopeNode currentFocus = FocusScope.of(context);
                if (!currentFocus.hasPrimaryFocus &&
                    currentFocus.focusedChild != null) {
                  FocusManager.instance.primaryFocus!.unfocus();
                }
              },
              child: Container(
                padding: EdgeInsets.only(left: 10, right: 10
                ),
                // margin: EdgeInsets.only(top: Constants.avatarRadius),
                decoration: BoxDecoration(
                    shape: BoxShape.rectangle,
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10),
                    boxShadow: [
                      BoxShadow(color: Colors.black,offset: Offset(0,10),
                          blurRadius: 10
                      ),
                    ]
                ),
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          IconButton(
                            icon: const Icon(Icons.clear, color: AppColors.colorBlack, size: 28,),
                            onPressed: () {
                              setState(() {
                               // Navigator.of(context).pop();
                                navigateToBookAppPage(context);
                              });
                            },
                          ),
                        ],
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left:20,right: 20),
                        child: Align( alignment: Alignment.topLeft,
                            child: Text(AppConstants.selectCityText, style: HeadlineTextStyle,)),
                      ),          Padding(
                        padding: const EdgeInsets.only(left: 20.0,right: 20),
                        child: DropdownButtonFormField(
                          icon:  Image(
                              image: AssetImage(
                                  ImagePath.bookAppointmentDropdownOrangeArrow)),
                          iconEnabledColor: AppColors.colorOrange,
                          hint: Text(
                            citySelection != '' ? citySelection:
                            AppConstants.selectCityText,
                            style: userPwdTextStyle,
                          ),
                          // value: citySelection,
                          items: model.citySelection!.map((CitySelection item) {
                            return DropdownMenuItem<int>(
                              child: Text(
                                item.city,
                                style:userPwdTextStyle,
                              ),
                              value: item.value,
                            );
                          }).toList(),
                          onChanged: (value) {
                            model.isLoading = true;
                            print(value);
                            int cityId =  value as int;
                            citySelection = model.citySelection![value-1].city;

                            model.cityAreaSelection.clear();
                            model.getRegions(cityId);
                            setState(() {
                              model.timeSlot == false;
                            regionSelection = '';
                              selectedTimeSlot = -1;
                              selectedDate == '';
                              selectedDateInDateFormat = DateTime.now();
                              model.timeSlotSelection.clear();
                            });

                            },
                          decoration: InputDecoration(
                            hintText: '',

                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 20.0,left:20,right: 20),
                        child: Align( alignment: Alignment.topLeft,
                          child:  Text(AppConstants.selectRegionText, style: HeadlineTextStyle,),
                        ), ),
                      Padding(
                        padding: const EdgeInsets.only(left: 20.0,right: 20),
                        child: DropdownButtonFormField(
                          icon:  Image(
                              image: AssetImage(
                                  ImagePath.bookAppointmentDropdownOrangeArrow)),
                          iconEnabledColor: AppColors.colorOrange,
                          hint: Text(
                            regionSelection != '' ? regionSelection:
                            AppConstants.selectRegionText,
                            style: userPwdTextStyle,
                          ),
                          items: model.cityAreaSelection.map((CityAreaSelection item) {
                            return DropdownMenuItem<int>(
                              child: Text(
                                item.regionName,
                                style: userPwdTextStyle,
                              ),
                              value: item.value,//regionSelection,
                            );

                          }).toList(),
                          onChanged: (value) {
                            int? index = (value as int?)!;
                            int regionId = model.cityAreaSelection[index].regionId;
                            regionSelection = model.cityAreaSelection[index].regionName;
                            setState(() {
                              model.timeSlot == false;
                              selectedDate == '';
                              selectedDateInDateFormat = DateTime.now();
                              selectedTimeSlot = -1;
                            });
                           // model.timeSlotSelection.clear();
                            model.getDivisionId(regionId);

                            selectedDate = DateFormat('dd-MM-yyyy').format( DateTime.now()) + ' ';
                            selectedDateInDateFormat =  DateTime.now();
                            setState(() {
                              selectedTimeSlot = -1;
                            });
                            model.timeSlot = true;
                            model.timeSlotSelection.clear();
                            model.getTimeStamp(selectedDate, _provider.selDivisionId);

                            print('value $value');
                          },
                          decoration: InputDecoration(
                            hintText: '',
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 20.0,left:20,right: 20, bottom: 10),
                        child: Align( alignment: Alignment.topLeft,child: Text(AppConstants.chooseDateText, style: HeadlineTextStyle,)),
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 10, right: 10),
                        height: 300,
                        decoration: BoxDecoration(
                          shape: BoxShape.rectangle,
                          color: AppColors.containerGrey,
                          // borderRadius: BorderRadius.circular(10),

                        ),
                        child: CalendarDatePicker(
                          onDateChanged: (DateTime value) {
                            model.isLoading = true;
                            selectedDate = DateFormat('dd-MM-yyyy').format(value) + ' ';
                            selectedDateInDateFormat = value;
                            setState(() {
                              selectedTimeSlot = -1;
                            });
                            model.timeSlot = true;
                            model.timeSlotSelection.clear();
                            model.getTimeStamp(selectedDate, _provider.selDivisionId);

                          },
                          lastDate: DateTime.now().add(Duration(days: 30)),
                          firstDate: DateTime.now(),
                          initialDate: selectedDateInDateFormat,//DateTime.now(),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 20.0,left:20,right: 20, bottom: 10),
                        child: Align(alignment: Alignment.topLeft,child: Text(AppConstants.chooseTimslotText, style: HeadlineTextStyle, textAlign: TextAlign.left,)),
                      ),
                      Container(
                        margin: EdgeInsets.only(bottom: 30),
                        width: MediaQuery.of(context).size.width*0.80,
                        height: 130,
                        child:(model.timeSlotSelection.isEmpty && model.timeSlot == true)?
                            Center(child: Padding(
                              padding: const EdgeInsets.all(16.0),
                              child: Text('No time slots found'),
                            ))

                        :GridView.builder(
                          shrinkWrap: false,
                          scrollDirection: Axis.vertical,
                          itemCount: model.timeSlotSelection.length,
                          gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: 3,
                            crossAxisSpacing: 10.0,
                            mainAxisSpacing: 10.0,
                            childAspectRatio:  (itemWidth / itemHeight),
                          ),
                          padding: EdgeInsets.only(left: 10, right: 10),
                          primary: false,
                          itemBuilder: (BuildContext context, int index) {

                            return GestureDetector(
                              onTap: () {
                                setState(() {
                                  selectedTimeSlot = index;
                                  timeSlotValue ='';
                                  timeSlotValue = model.timeSlotSelection[index].timeSlot;
                                  model.slotId = model.timeSlotSelection[index].slotId;
                                  print('index $index');
                                  NavigationModel navigationModel = NavigationModel(citySelection, regionSelection, selectedDate, timeSlotValue, selectedTimeSlot, selectedDateInDateFormat,
                                      _provider.selDivisionId, _provider.cityAreaSelection, _provider.timeSlotSelection, _provider.slotId, widget.token, widget.password, widget.attendeeList,
                                      widget.contactPersonList, widget.selectedCP, widget.buildingType, widget.addressType, widget.buildingName, widget.cityState, widget.landmark, widget.navigationPage);

                                  widget.callbackFunction(navigationModel);
                                  navigateToBookAppPage(context);

                                });
                              },
                              child: Container(
                                  alignment: FractionalOffset.center,
                                  decoration: BoxDecoration(
                                    shape: BoxShape.rectangle,
                                    color: selectedTimeSlot == index? AppColors.blueGreyColor: AppColors.colorLightBlue,
                                    borderRadius: BorderRadius.circular(15),

                                  ),
                                  child: Text(model.timeSlotSelection[index].timeSlot,
                                      style: selectedTimeSlot == index? HeadlineWhiteTextStyle: HeadlineTextStyle)
                              ),
                            );

                          },
                        ),
                      ),


                    ],
                  ),
                ),
              ),
            ),
          );
        },
        ),
      ),
    );

  }


  navigateToBookAppPage(BuildContext context) {

    Navigator.of(context).pop();
   /* Navigator.push(
        context, MaterialPageRoute(builder: (context) => BookNewAppPackageView(
        citySelection: citySelection ,
        regionSelection: regionSelection ,
        selectedDate: selectedDate,
        timeSlotValue: timeSlotValue,
        selectedTimeSlot: selectedTimeSlot,
        selectedDateInDateFormat :selectedDateInDateFormat,
      divisionId :_provider.selDivisionId,
      regionList : _provider.cityAreaSelection,
      timeSlotList: _provider.timeSlotSelection,
      slotId: _provider.slotId,
      token: widget.token,
      password: widget.password,
      attendeeList: widget.attendeeList,
      contactPersonList: widget.contactPersonList,
      selectedCP: widget.selectedCP,
      buildingType: widget.buildingType,
      addressType: widget.addressType,
      buildingName: widget.buildingName,
      cityState: widget.cityState,
      landmark: widget.landmark,
      navtigationPage: widget.navigationPage,
    )));*/
  }

}