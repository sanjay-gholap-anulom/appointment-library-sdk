

import 'dart:async';
import 'dart:convert';

import 'package:book_appointment_package/src/utils/api_constants.dart';
import 'package:book_appointment_package/src/utils/app_constants.dart';
import 'package:book_appointment_package/src/utils/sharedpreference_utils.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'custom_dialog_box_provider.dart';
import 'model/book_app_attendee_db_model.dart';
import 'model/credit_blance_response_model.dart';

class BookNewAppProvider extends ChangeNotifier{

  String tokenh = 'DOtUBMhv5pk51tl0D37uBcezq85cXNN7hZQ7';

  List<BookAppAttendeeDBModel> _contactPersonList = [
   ];

  BookNewAppProvider(){
    checkEmail();
  }

  List<BookAppAttendeeDBModel> get contactPersonList => _contactPersonList;


  set contactPersonList(List<BookAppAttendeeDBModel> value) {
    _contactPersonList = value;
  }
  BookAppAttendeeDBModel? selectedContactPerson = BookAppAttendeeDBModel(name: '', contact: '', email: '') ;
  String _selectedCPName = '';

  String get selectedCPName => _selectedCPName;

  set selectedCPName(String value) {
    _selectedCPName = value;
    notifyListeners();
  }

  List<BookAppAttendeeDBModel> _addAttendeeList = [];

  List<BookAppAttendeeDBModel> get addAttendeeList => _addAttendeeList;

  set addAttendeeList(List<BookAppAttendeeDBModel> value) {
    _addAttendeeList = value;
  }

  int _groupBuildingType = 0;

  int get groupBuildingType => _groupBuildingType;

  set groupBuildingType(int value) {
    _groupBuildingType = value;
    notifyListeners();
  }

  int _groupAddressType = 0;

  int get groupAddressType => _groupAddressType;

  set groupAddressType(int value) {
    _groupAddressType = value;
    notifyListeners();
  }

  bool _showDiffAddress = false;

  bool get showDiffAddress => _showDiffAddress;

  set showDiffAddress(bool value) {
    _showDiffAddress = value;
  }

  double _creditBalance =0;

  double get creditBalance => _creditBalance;

  set creditBalance(double value) {
    _creditBalance = value;
  }
  bool _isLoading= false;
  bool get isLoading => _isLoading;
  set isLoading(bool value) {
    _isLoading = value;
    notifyListeners();
  }
  int _appointmentCharges = 0;


  int get appointmentCharges => _appointmentCharges;

  set appointmentCharges(int value) {
    _appointmentCharges = value;
  }

  int? _amountToPay = 0;

  int? get amountToPay => _amountToPay;

  set amountToPay(int? value) {
    _amountToPay = value;
  }
  NavigationModel _navigationModel = NavigationModel('', '', '', '', -1, DateTime.now(), 0, [], [], 0, '', '', [], [], '', 0, 0, '', '', '', '');

  NavigationModel get navigationModel => _navigationModel;

  set navigationModel(NavigationModel value) {
    _navigationModel = value;
  }

  bool _obscureText = true;
  bool get obscureText => _obscureText;
  set obscureText(bool value) {
    _obscureText = value;
    notifyListeners();
  }

  int itemWidth = 91;
  int itemHeight = 38;

  List<PartyTypeSelection> partyTypeList = [
    new PartyTypeSelection(0, "Owner", 1),
    new PartyTypeSelection(1, "Tenant",2),
    new PartyTypeSelection(2,  "POA", 4),
    new PartyTypeSelection(3, "Customer Witness", 3),
    new PartyTypeSelection(4, "Anulom Witness", 5),
  ];
   String _selPartyName ='';

  String get selPartyName => _selPartyName;

  set selPartyName(String value) {
    _selPartyName = value;
  }

  int _selPartyType = 0;

  int get selPartyType => _selPartyType;

  set selPartyType(int value) {
    _selPartyType = value;
  }
  String _email ='';


  String get email => _email;

  set email(String value) {
    _email = value;
  }

  String _appDate = '';

  String get appDate => _appDate;

  set appDate(String value) {
    _appDate = value;

  }

  checkEmail() async{
    _email = (await SharedPreferenceUtils.getStringPreference(
        SharedPreferenceUtils.EMAIL_ID)) ?? '';
    print('emailp $_email');
    notifyListeners();
  }

  void addAttendeeToList(int partyType, String env, String name, String email, String contact, String address ){
    _addAttendeeList.add(new BookAppAttendeeDBModel( partyType: partyType, env: '', name: name, email: email, contact: contact, address: address));
    _contactPersonList.add(new BookAppAttendeeDBModel(partyType: partyType, env: '', name: name, email: email, contact: contact, address: address));

    notifyListeners();
  }

  void editOtherAttendee(int index,  String env, int partyType, String name, String email, String contact, String address){
    print('partyType2 $partyType');
    _selectedCPName='';

    _addAttendeeList.removeAt(index);
    _addAttendeeList.insert(index, new BookAppAttendeeDBModel(partyType: partyType, env: '', name: name, email: email, contact: contact, address: address));
    updateSelectContactPersonList();
    notifyListeners();
  }

  void deleteOtherAttendee(int index, String name){

    _addAttendeeList.removeAt(index);
    _selectedCPName = '';
    updateSelectContactPersonList();
    notifyListeners();
  }

  void updateSelectContactPersonList(){
    _contactPersonList.clear();
    print('list ${ _contactPersonList.length}  ${_addAttendeeList.length}');
     _contactPersonList.addAll(_addAttendeeList);
    print('list ${ _contactPersonList.length}  ${_addAttendeeList.length}  $_selectedCPName');

  }

  Future<ChargesDetailsResponseModel?> getChargesDetails(String city, String region) async{

    String? _token = await SharedPreferenceUtils.getStringPreference(SharedPreferenceUtils.AUTH_TOKEN)?? tokenh ;
    String? _email = await SharedPreferenceUtils.getStringPreference(SharedPreferenceUtils.EMAIL_ID) ?? email;

    final uri = Uri.parse(Api.CHARGES_DETAILS+ '?' +
        AppConstants.getAuthToken + _token + '&' +
        AppConstants.EMAIL +
        _email+ '&'+
        AppConstants.CITY +
        city +'&'+
        AppConstants.REGION +
        region
    );

    print('ChargesDetails uri - $uri');

    try {
      final response = await http.get(uri);

      print('ChargesDetails response status = ${response.statusCode}');

      if (response.statusCode == 200) {
        print('ChargesDetails response ==' + response.body);

        if (response.body.isNotEmpty) {
          var responseModel =
          ChargesDetailsResponseModel.fromJson(
              jsonDecode(response.body));
          print('app charges ${responseModel.appointmentCharges}');

          if(responseModel.creditBalance == null){
         //   await insertFailedApi(uri.toString(), "", response.body.toString());
          }
          return responseModel;
        }

      } else {
        //await insertFailedApi(uri.toString(), "", response.body.toString());
        print('server side error');
      }
    }
    on TimeoutException catch (e) {
      print('server side error');
      // CustomToast().customToast('server side error');
    } on Error catch (e) {
      //  CustomToast().customToast('server side error');
    }

  }
 /* Future<bool?> addBookAppointmentDetails(BookAppointmentDBModel bookAppointmentDBModel) async{
    DBOperations _db = DBOperations();
    print('db $_db');
    await _db.createUpdateBookAppDetails(bookAppointmentDBModel , 1);
  await _db.createUpdateBookAppAttendee( _contactPersonList);
  }*/
}
class ContactPersonSelection{
  int value;
  String contactPerson;
  ContactPersonSelection(this.value, this.contactPerson);
}

class PartyTypeSelection{
  int value;
  String party_name;
  int party_type;

  PartyTypeSelection(this.value, this.party_name, this.party_type);
}
class NavigationModel {
  String citySelection;
  String regionSelection;
  String selectedDate;
  String timeSlotValue;
  int selectedTimeSlot;
  DateTime selectedDateInDateFormat;
  int divisionId ;
  List<CityAreaSelection> regionList;
  List<TimeslotSelection> timeSlotList;
  int slotId;
  String token;
  String password;
  List<BookAppAttendeeDBModel> attendeeList;
  List<BookAppAttendeeDBModel> contactPersonList;
  String selectedCP;
  int buildingType;
  int addressType;
  String buildingName;
  String cityState;
  String landmark;
  String navtigationPage;

  NavigationModel(
      this.citySelection,
      this.regionSelection,
      this.selectedDate,
      this.timeSlotValue,
      this.selectedTimeSlot,
      this.selectedDateInDateFormat,
      this.divisionId,
      this.regionList,
      this.timeSlotList,
      this.slotId,
      this.token,
      this.password,
      this.attendeeList,
      this.contactPersonList,
      this.selectedCP,
      this.buildingType,
      this.addressType,
      this.buildingName,
      this.cityState,
      this.landmark,
      this.navtigationPage);
}