import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:book_appointment_package/src/utils/api_constants.dart';
import 'package:book_appointment_package/src/utils/app_constants.dart';
import 'package:book_appointment_package/src/utils/custom_toast.dart';
import 'package:book_appointment_package/src/utils/sharedpreference_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';

import 'model/city_selection_response_model.dart';
import 'model/region_selection_response_model.dart';
import 'model/time_slot_response_model.dart';


class CustomDialogBoxProvider extends ChangeNotifier{
  CustomDialogBoxProvider(){
    getCities();
  }

  bool _timeSlot = false;

  bool get timeSlot => _timeSlot;

  set timeSlot(bool value) {
    _timeSlot = value;
    notifyListeners();
  }

  List<CitySelection>? _citySelection = [
    ];

  List<CitySelection>? get citySelection => _citySelection;

  set citySelection(List<CitySelection>? value) {
    _citySelection = value;
  }

  List<CityAreaSelection> _cityAreaSelection = [
   ];

  List<CityAreaSelection> get cityAreaSelection => _cityAreaSelection;

  set cityAreaSelection(List<CityAreaSelection> value) {
    _cityAreaSelection = value;
  }
  List<TimeslotSelection> _timeSlotSelection = [];

  List<TimeslotSelection> get timeSlotSelection => _timeSlotSelection;

  set timeSlotSelection(List<TimeslotSelection> value) {
    _timeSlotSelection = value;
    notifyListeners();
  }
  String merchant_nameh = "Ramm";
  int platform_id = 17;
  String tokenh = 'DOtUBMhv5pk51tl0D37uBcezq85cXNN7hZQ7';

 late int _selDivisionId ;

  set selDivisionId(int value) {
    _selDivisionId = value;
  }

  int get selDivisionId => _selDivisionId;

  bool _isLoading= true;
  bool get isLoading => _isLoading;
  set isLoading(bool value) {
    _isLoading = value;
    notifyListeners();
  }
  void getDivisionId(int regionId){
    for(int i=0; i<_cityAreaSelection.length; i++){
      if(_cityAreaSelection[i].regionId == regionId){
        _selDivisionId = _cityAreaSelection[i].divisionId;
        print('_selDivisionId $_selDivisionId');
      }
    }
  }
  int _slotId = 0;

  int get slotId => _slotId;

  set slotId(int value) {
    _slotId = value;
  }


  String version = '1.0.0';

  Future<void> getCities() async {
     String? token = await SharedPreferenceUtils.getStringPreference(
        SharedPreferenceUtils.AUTH_TOKEN)?? tokenh;


    final uri = Uri.parse(
        Api.GET_CITY+'?' +
            AppConstants.getAuthToken +
            token);

    print('getcity uri - $uri');
     try {
    final response = await http.get(uri);

    print('getcity response status = ${response.statusCode}');

    if (response.statusCode == 200) {
      var citySelectionResponseModel = CitySelectionResponseModel.fromJson(
          jsonDecode(response.body.toString()));

      if (response.body.isNotEmpty) {

        for (int i = 0; i < citySelectionResponseModel.city!.length; i++) {
          var id = citySelectionResponseModel.city![i].id!;
          var city = citySelectionResponseModel.city![i].cityName!;
         _citySelection!.add(CitySelection(id, city));
        }

      }
    }
    else{
      CustomToast().customToast('Something went wrong');
    }
     }on TimeoutException catch (e) {
       print('server side error');
       CustomToast().customToast('server side error');

     } on SocketException catch (e) {
       print('message ${e.message}');
      // CustomToast().customToast(AppConstants.pleaseCheckYourInternetConnection);
       CustomToast().customToast('server side error');
     }
     _isLoading = false;

     notifyListeners();
  }
  Future<void> getRegions(int cityId) async {
    String? token = await SharedPreferenceUtils.getStringPreference(
        SharedPreferenceUtils.AUTH_TOKEN)?? tokenh;
    String timestamp = DateFormat('yyyy-dd-MM hh:mm:ss').format(DateTime.now());
    print('timestamp $timestamp');
    final uri = Uri.parse(
        Api.GET_REGIONS +'?' +
            AppConstants.getAuthToken +
            token +
            '&' +
            AppConstants.CITYID +
            cityId.toString() +
            '&' +
            AppConstants.TIMESTAMP +
            timestamp);

    print('getregion uri - $uri');

    final response = await http.get(uri);
    print('getregion response status = ${response.statusCode}');

    if (response.statusCode == 200) {
      var regionResponseModel = RegionResponseModel.fromJson(
          jsonDecode(response.body.toString()));
      if (response.body.isNotEmpty) {
        for (int i = 0; i < regionResponseModel.regions!.length; i++) {
          var regionId = regionResponseModel.regions![i].regionId;
          var regionName = regionResponseModel.regions![i].regionName;
          var divisionId = regionResponseModel.regions![i].divisionId;
          if(divisionId != null) {
            cityAreaSelection.add(new CityAreaSelection(i, regionId!, regionName!, divisionId));
          }
        //  print('region ${cityAreaSelection[i].regionName}');
        }
      }

    }
    else{

      CustomToast().customToast('Something went wrong');
    }
    _isLoading = false;
    notifyListeners();
  }

  Future<void> getTimeStamp(String appointmentDate, int divisionId) async {
    String? token = await SharedPreferenceUtils.getStringPreference(
        SharedPreferenceUtils.AUTH_TOKEN)?? tokenh;
    String? merchant_name= await SharedPreferenceUtils.getStringPreference(
        SharedPreferenceUtils.MERCHANT_NAME)?? merchant_nameh;
    int? platformId = await SharedPreferenceUtils.getIntPreferences(
        SharedPreferenceUtils.PLATFORM_ID)?? 17;

    final uri = Uri.parse(Api.GET_TIME_SLOT + '?'+
        AppConstants.getAuthToken +
        token +
        '&' +
        AppConstants.APPDATE +
        appointmentDate +
        '&' +
        AppConstants.DIVISIONID +
        divisionId.toString() +
        '&' +
        AppConstants.MERCHANTNAME +
        merchant_name +

        '&' +
        AppConstants.PLATFORMID +
        platformId.toString());

    print('gettimeslot uri - $uri');

    final response = await http.get(uri);
    print('status ${response.statusCode}');

    if (response.statusCode == 200) {
      var timeSlotResponseModel =  TimeSlotResponseModel.fromJson(jsonDecode(response.body.toString()));
      if (response.body.isNotEmpty) {
        for (int i = 0; i < timeSlotResponseModel.timeSlot!.length; i++) {
          var startTime = timeSlotResponseModel.timeSlot![i].cStartTime;
          var endTime = timeSlotResponseModel.timeSlot![i].cEndTime;
          var slotId = timeSlotResponseModel.timeSlot![i].slotId;
          _timeSlotSelection.add(new TimeslotSelection(i+1, "$startTime", slotId!));
          // print("$startTime - $endTime");
        }
        print("$_timeSlotSelection");
      }

    }
    else{
      CustomToast().customToast('Something went wrong');
    }
    _isLoading = false;
    notifyListeners();
  }

}


class CitySelection{
  int value;
  String city;
  CitySelection(this.value, this.city);
}

class CityAreaSelection{
  int value;
  int regionId;
  String regionName;
  int divisionId;
  CityAreaSelection(this.value,this.regionId, this.regionName, this.divisionId);
  //CityAreaSelection(this.value, this.regionName);
}

class TimeslotSelection{
  int value;
  String timeSlot;
  int slotId;
  TimeslotSelection(this.value, this.timeSlot, this.slotId);
}
